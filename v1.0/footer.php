<div class="clear"></div>
Copyright &#169; Management Information System (MIS),
Directorate General of Health Services (DGHS),
Mohakhali, Dhaka-1212
<!-- <span class="toggleBtn" style="margin-top: 20px;cursor: pointer">[+] Click to see debug values</span>-->

<div class="clear"></div>
<span class="toggleBtn" style="margin-top: 20px;cursor: pointer">[+] Click to see debug values</span>
<div class="debug" style="display: none">

    <?php
    if ($debug) {
        echo "Following information is only for software developers for testing this application.<br>===================<br>";
        echo "SESSION<br>===================<br>";
        myprint_r($_SESSION);
        echo "REQUEST<br>===================<br>";
        myprint_r($_REQUEST);
        echo "FILES<br>===================<br>";
        myprint_r($_FILES);

        echo "<code>Query : $q_filtered<br></code>";
        myprint_r($dataArray);
    }
    ?>
</div>
<script type="text/javascript">
    $('document').ready(function() {
        $('.toggleBtn').click(function() {
            $('div[class=debug]').toggle();
        });
    });
</script>

<?php ob_end_flush(); ?>
