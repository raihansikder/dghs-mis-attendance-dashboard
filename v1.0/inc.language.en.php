<?php
$lang['']='';

$lang['org_code']='Org Code';
$lang['org_hrm_name']='Organization Name';
$lang['org_hrm_division_name']='Division';
$lang['org_hrm_district_name']='District';
$lang['org_hrm_upazila_name']='Upazila';
$lang['org_hrm_org_type_name']='Org Type';
$lang['org_hrm_org_level_name']='Org Level';
$lang['org_hrm_org_photo']='Photo';
$lang['org_hrm_land_phone1']='Land phone1';
$lang['org_hrm_mobile_number1']='Mobile1';
$lang['org_hrm_email_address1']='Email1';
$lang['totalEmployee']='Total Employee';
$lang['totalAttendance']='Total Attendance';
$lang['machine_status']='Machine Status';

$lang['attendence_details_table_header']="";

function locale($LanguageString){
	global $lang;
	return $lang[$LanguageString];
}

?>