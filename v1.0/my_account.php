<?php
include_once("config.php");
/* * **************************************** */
/* Update my account information
  /****************************************** */
$valid = true;
$alert = array();
$param = $_REQUEST['param'];
$account_id = $_SESSION['current_user_id'];
$new_password = $_REQUEST['new_password'];

if ($_REQUEST['first_login'] == '1') {
  setRowFieldVal('account', 'account_last_login', getDateTime(), 'account_id', $_SESSION[current_user_id]);
}

if (isset($_POST[submit])) {
  if (strlen($new_password)) {
    if (setRowFieldVal("account", "account_password", $new_password, "account_id", $account_id)) {
      insertLog('Account Management', 'My Account password updated', 'account', 'account_id', $account_id, $sql, $_SESSION[current_user_id], "New password - $new_password |" . print_r($_SERVER, true));
      $password_change_success = true;
      array_push($alert, "Your password has been changed");
    } else {
      $valid = false;
      array_push($alert, "Error saving password");
      $password_change_success = false;
    }
  }
}
if ($_SESSION[current_user_type_name] == 'Customer') {
  $employee_am = getRowVal('employee', 'employee_id', $_SESSION[details][customer_account_manager_id]); // get whole row for account manager
} else if ($_SESSION[current_user_type_name] == 'User') {
  $customer_id = $_SESSION[details][user_customer_id];
  $customer_account_manager_id = getAccoutManagerIdFrmCustomerId($customer_id);
  $employee_am = getRowVal('employee', 'employee_id', $customer_account_manager_id); // get whole row for account manager
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <?php include_once('inc.head.php') ?>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  </head>
  <body>
    <div id="wrapper">
      <div id="container">
        <div id="top1">
          <?php include('top.php'); ?>
        </div>
        <div id="mid">
          <?php
          if (isset($_POST[submit])) {
            echo "<span style='font-size:18px;'>";
            printAlert($valid, $alert);
            echo "</span>";
          }

            ?>
            <h1>My Account</h1>
            <div class="clear"></div>
            <form action="<?= $_SERVER['PHP_SELF']; ?>" method="post">
              <table>
                <tr>
                  <td>
                    Change password: <br />
                    <input name="new_password" type="password" value="" size="30" maxlength="60" class="validate[required]" />
                  </td>
                </tr>
                <tr>
                  <td>
                    <input name="submit" type="submit" value="Save" class="bgblue button" />
                  </td>
                </tr>
              </table>
              <!--
              <div class="clear"></div>
              <div class="terms_and_conditions" style="width:500px; height:300px; overflow:scroll">
              <?php include('terms_and_condition.php'); ?>
              </div>
              -->
              <?php if ($_SESSION[current_user_type_name] == 'Voice Artist') { ?>
                <div class="clear"></div>
                <input type="checkbox" name="accept" value="accept" class="validate[required]"/>I accept the <a href="http://www.premierba.co.uk/bd/wp-content/uploads/2013/09/Voice-Artist-T-and-Cs-16th-September-20131.pdf" target="_blank">terms and conditions</a> of using this website.
              <?php } ?>
            </form>
            <div class="clear"></div>

          <?php
          if ($_SESSION[current_user_type_name] == 'Customer' || $_SESSION[current_user_type_name] == 'User') {
            echo "For assistance please contact your account manager: <br><b>" . $employee_am[employee_full_name] . "</b> <br> " . $employee_am[employee_telephone] . " <br> <a href='mailto:" . $employee_am[employee_email] . "'>" . $employee_am[employee_email] . "</a><br>";
          }
          ?>
        </div>
        <div id="footer">
          <?php include('footer.php'); ?>
        </div>
      </div>
    </div>
  </body>
</html>
