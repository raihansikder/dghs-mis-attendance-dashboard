<?php
include_once("config.php");
$parameterized_query = " 1 ";
if (count($_REQUEST['datadivision'])) {
    //$datadivision_csv = implode(',', $_REQUEST['datadivision']);
    $datadivision_csv = "'" . implode("','", $_REQUEST['datadivision']) . "'";
    $parameterized_query.=" AND division_name in ($datadivision_csv)";
}
if (count($_REQUEST['dataIndicator'])) {
    $dataIndicator_csv = implode(',', $_REQUEST['dataIndicator']);
    //$parameterized_query.=" AND division_name in ($dataIndicator_csv) ";
}

$year = $_REQUEST['dataYear'];
if (!strlen($year)) {
    $year = '2013';
}
$month = $_REQUEST['dataMonth'];
if (!strlen($month)) {
    $month = '';
}
//echo $dataIndicator_csv;
$dbTableName = "data_bd_division_" . $year;
if (strlen($month))
//$dbTableName = "data_bd_division_$year_m$month";
    $dbTableName = "data_bd_division_" . $year . "_m" . $month;

//$q_filtered = "SELECT * FROM data_bd_division_" . $year . "_m" . $month. " WHERE ".$parameterized_query;
$q_filtered = "SELECT * FROM " . $dbTableName. " WHERE ".$parameterized_query;



$r = mysql_query($q_filtered) or die(mysql_error() . "<br>Query<br>____<br>$q_filtered<br>");

$rows = mysql_num_rows($r);
if ($rows > 0) {
    $dataArray = mysql_fetch_rowsarr($r);
    // myprint_r($dataArray);
}

$divisionCount = 7;
if (count($_REQUEST['datadivision'])) {
    $divisionCount = count($_REQUEST['datadivision']);
}
$chartHeight = calculateChartHeight($divisionCount, count($_REQUEST['dataIndicator']));
$FirstRowVal = "'division'";
$legend = array();
foreach ($_REQUEST['dataIndicator'] as $dataIndicator) {
    $FirstRowVal.= ", '".str_replace('_', ' ', $dataIndicator)."'";
    array_push($legend, $dataIndicator);
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <?php include_once('inc.head.php') ?>
        <style>
            th{
                width: 40px;
            }
        </style>
    </head>
    <body>
        <div id="wrapper">
            <div id="container">
                <?php include_once('top.php'); // static top menu    ?>
                <div id="mid">
                    <div class="filterContainer">
                      <?php include_once 'snippets/common/inc.filter.form.data_district.php'; ?>
                    </div>
                    <?php if (isset($_REQUEST['submit']) && $rows > 0) : ?>
                    <div class="clear"></div>
                    <div class="row">
                        <ul class="nav nav-tabs nav-justified">
                            <li class="active"><a href="#barChart" data-toggle="tab">BAR CHART</a></li>
                            <li><a href="#dataTable" data-toggle="tab">TABULAR DATA</a></li>
                            <li><a href="#pieChart" data-toggle="tab">PIE CHART</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="barChart">
                                <h3><span class="glyphicon glyphicon-stats"></span> BAR CHART</h3>
                                <div id="chart_div" style="width: 900px; height: <?= $chartHeight ?>px; position: relative"></div>
                                <script type="text/javascript" src="https://www.google.com/jsapi"></script>
                                <script type="text/javascript">
                                    google.load("visualization", "1", {packages: ["corechart"]});
                                    google.setOnLoadCallback(drawChart);
                                    function drawChart() {
                                        var data = google.visualization.arrayToDataTable([
                                            [<?= $FirstRowVal ?>]
                                        <?php
                                        foreach ($dataArray as $data) {
                                            echo ",[";
                                            echo "'" . $data['division_name'] . "'";
                                            foreach ($_REQUEST['dataIndicator'] as $dataIndicator) {
                                                if (strlen($data[$dataIndicator])) {
                                                    echo "," . $data[$dataIndicator];
                                                } else {
                                                    echo ", 0";
                                                }
                                            }
                                            echo "]";
                                        }
                                        ?>

                                        ]);

                                        var options = {
                                            title: 'DGHS MIS - Data Analyzer',
                                            chartArea: {left: 150, top: 0, width:"50%", height: "80%"},
                                            vAxis: {title: '<?= $year ?>', titleTextStyle: {color: 'red'}}

                                        };

                                        var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
                                        chart.draw(data, options);
                                    }
                                </script>
                            </div>
                            <div class="tab-pane fade" id="pieChart">
                                <h3><span class="glyphicon glyphicon-stats"></span> PIE CHART</h3>
                                <?php
                                if($divisionCount == 1){
                                echo "<div class='alert alert-danger'>Please select multiple administrative units to view administrative unit wise pie chart data representation.</div>";
                                }
                                else {
                                foreach ($_REQUEST['dataIndicator'] as $dataIndicator) {
									$chartTitle = str_replace('_', ' ', $dataIndicator);
									$chartTitle = str_replace('no', 'No ', $chartTitle);
                                    drawPieChartFromIndicator($divName = 'piechart_' . $dataIndicator, $chartTitle = $chartTitle, $col1Title = 'division', $col2Title = 'Count', $dataArray, $dataIndicator);
                                }
                                }

                                ?>
                            </div>
                            <div class="tab-pane fade" id="dataTable">
                                <h3> <span class="glyphicon glyphicon-list-alt"></span> TABULAR DATA</h3>

                                <table id="datatable_tableView" width="100%">
                                    <thead>
                                        <tr>
                                            <th>division</th>
                                            <?php
                                            foreach ($_REQUEST['dataIndicator'] as $dataIndicator) {
												$chartTitle = str_replace('_', ' ', $dataIndicator);
												$chartTitle = str_replace('no', 'No ', $chartTitle);
                                                echo "<th>" . $chartTitle. "</th>";
                                            }
                                            ?>

                                        </tr>
                                        <tr class="filterInput">
                                            <td><input type="text" name="division" value="" class="search_init" /></td>
                                            <?php
                                            foreach ($_REQUEST['dataIndicator'] as $dataIndicator) {
                                                echo "<td><input type=\"text\" name=\"$dataIndicator\" value=\"\" class=\"search_init\" /></td>";
                                            }
                                            ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($dataArray as $data) { ?>
                                            <tr>
                                                <td>
                                                    <?= $data['division_name'] // division_name = division  ?>
                                                </td>
                                                <?php
                                                foreach ($_REQUEST['dataIndicator'] as $dataIndicator) {
                                                    echo "<td>" . intval($data[$dataIndicator]) . "</td>";
                                                }
                                                ?>

                                            </tr>
                                        <?php }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                    <?php else:
                        echo "<div class='alert'>No data found</div>";
                    ?>
                    <?php endif; ?>

                </div>
                <div id="footer">
                    <div class="clear"></div>
                    <?php
                    include('footer.php');
                    ?>
                </div>

            </div>
        </div>
    </body>
</html>
<script type="text/javascript">
    $('document').ready(function() {
        $('#mainnav li#overview').addClass('active');
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#topLeftMenu #product_list').addClass('active');
        oTable = $('#datatable_tableView').dataTable({
            "bPaginate": false,
            "sPaginationType": "full_numbers",
            "iDisplayLength": 25,
            "bStateSave": false,
            "oLanguage": {
                "sSearch": "Search all columns:"
            },
            "bSortCellsTop": true
        });

        /*Datatable sort*/
        $("thead input").keyup(function() {
            /* Filter on the column (the index) of this element */
            oTable.fnFilter(this.value, $("thead input").index(this));
            var index = $("thead input").index(this);
            index++;
            //alert(index);
            $("#datatable_tableView tbody tr td:nth-child(" + index + ")").removeHighlight();
            $("#datatable_tableView tbody tr td:nth-child(" + index + ")").highlight($(this).val());
        });
        /*
         * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
         * the footer
         */
        $("thead input").each(function(i) {
            //asInitVals[i] = this.value;
        });

        $("thead input").focus(function() {
            if (this.className == "search_init")
            {
                this.className = "search_init_focus";
                this.value = "";
            }
        });

        $("thead input").blur(function(i) {
            if (this.value == "")
            {
                this.className = "search_init";
                this.value = asInitVals[$("thead input").index(this)];
            }
        });

    });
</script>