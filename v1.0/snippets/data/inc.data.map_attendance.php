<style>
    .leaflet-top, .leaflet-bottom{z-index: 1}
</style>
<div id="mapContainer">
    <h3><span class="glyphicon glyphicon-globe"></span> MAP VIEW</span></h3>
    <div style=" height:850px" id="map"></div>
    <!--
            AWESOOME MARKERS
            https://github.com/lvoogdt/Leaflet.awesome-markers
    -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.0/css/font-awesome.css" rel="stylesheet" media="screen">
    <link href="http://watson.lennardvoogdt.nl/Leaflet.awesome-markers/dist/leaflet.awesome-markers.css" rel="stylesheet" media="screen">
    <script src="http://watson.lennardvoogdt.nl/Leaflet.awesome-markers/dist/leaflet.awesome-markers.js"></script>
    <!--<script src="https://rawgithub.com/jseppi/Leaflet.MakiMarkers/master/Leaflet.MakiMarkers.js"></script>-->


    <script type='text/javascript'>
        var map = L.map('map').setView([23.91597, 90.197754], 8);
        L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors'

        }).addTo(map);
        map.scrollWheelZoom.disable();// disable mouse scroll zoom
        //new L.Control.Zoom({position: 'topright'}).addTo(map); // move zoom control to right
        /*
         var myIcon = L.icon({
         iconSize: [38, 95],
         iconAnchor: [22, 94],
         popupAnchor: [-3, -76],
         shadowUrl: 'my-icon-shadow.png',
         shadowRetinaUrl: 'my-icon-shadow@2x.png',
         shadowSize: [68, 95],
         shadowAnchor: [22, 94]
         });
         */

<?php
foreach ($dataArray as $data) {
    $hosid = $data['detail']['hosid'];
    //$hosname = $data['detail']['hosname'];
    $hosname = $data['detail']['org_hrm_name'];

    $long = $data['detail']['long'];
    $lat = $data['detail']['lat'];
    $totalAttendance = $data['totalAttendance'];
    $totalEmployee = $data['totalEmployee'];
    $attendancePercentage = round(($totalAttendance * 100 / $totalEmployee), 0);
    $popupContent = '';
    //if (strlen($lat) && strlen($long) && $totalEmployee > 0) {
    if (strlen($lat) && strlen($long)) {
        // Default marker setting
        $markerColor = 'red';
        $markerIcon = 'medkit';
        $AwesomeMarkerPrefix = 'fa';
        $AwesomeMarkerSpin = 'false';

        if (strlen(trim($data['machine_status_details']['machine_status_markerColor']))) {
            $markerColor = trim($data['machine_status_details']['machine_status_markerColor']);
        }

        if ($attendancePercentage > 0) {
            $AwesomeMarkerPrefix = 'glyphicon';
            $markerIcon = 'glyphicon-asterisk';
            $markerColor = 'green';

            //'red', 'darkred', 'orange', 'green', 'darkgreen', 'blue', 'purple', 'darkpuple', 'cadetblue'
            $AwesomeMarkerSpin = 'true';
        }



        /* Marker content
         */
        if (strlen($data['detail']['org_hrm_org_photo'])) {
            $popupContent.="<a target='_blank' href='$OrgRegistryPath?org_code=" . $data['detail']['org_code'] . "'><img src='" . $data['detail']['org_hrm_org_photo'] . "' height='50px';/></a>";
            $popupContent.="<span class='percentCount'>$attendancePercentage%</span><br/>";
            //$popupContent.="<a >Open in HRM</a><br/>";
        }
        $popupContent.="<b>$hosname</b><br/>$totalAttendance (current shift) out of $totalEmployee checked in<br/>";
        //$popupContent.="Percentage of attendance <b>$attendancePercentage %</b><br/>";
        $popupContent.= "<span class='label label-info pull-right' style='font-size:14px'>" . $data['machine_status_details']['machine_status_name'] . "</span></br>";
        /*
         */
        ?>
                var AwesomeMarker = L.AwesomeMarkers.icon({
                    icon: '<?= $markerIcon ?>',
                    prefix: '<?= $AwesomeMarkerPrefix ?>',
                    markerColor: '<?= $markerColor ?>',
                    spin: <?= $AwesomeMarkerSpin ?>
                });
        <?php
        //echo "L.marker([$lat, $long]).addTo(map).bindPopup(\"$popupContent\").openPopup();";

        if ($data['detail']['org_code'] == '10000002') {
            echo "L.marker([$lat, $long], {icon: AwesomeMarker}).addTo(map).bindPopup(\"$popupContent\").openPopup();\n";
        } else {
            echo "L.marker([$lat, $long], {icon: AwesomeMarker}).addTo(map).bindPopup(\"$popupContent\");\n";
        }
    }
}
?>

    </script>
</div>