<h3><span class="glyphicon glyphicon-stats"></span> PIE CHART</h3>
<?php
if ($districtCount == 1) {
  echo "<div class='alert alert-danger'>Please select multiple administrative units to view administrative unit wise pie chart data representation.</div>";
} else {
  foreach ($_REQUEST['dataIndicator'] as $dataIndicator) {
    $chartTitle = ucfirst(str_replace('_', ' ', $dataIndicator));
    //smyprint_r($dataArray);
    drawPieChartFromIndicator($divName = 'piechart_' . $dataIndicator, $chartTitle = $chartTitle, $col1Title = 'District', $col2Title = 'Count', $dataArray, $dataIndicator);
  }
}
?>