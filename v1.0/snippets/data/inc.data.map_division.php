<div id="mapContainer">
  <h3><span class="glyphicon glyphicon-globe"></span> MAP VIEW</span></h3>
  <div style=" height:500px" id="map"></div>
  <script type='text/javascript'>
    var map = L.map('map').setView([23.91597,90.197754], 7);
    L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors'
    }).addTo(map);

    <?php
    foreach ($dataArray as $data) {
    $popupContent = "<b>" . $data['name_3'] . "</b>";
    $popupContent.="<table style='font-size:8px;'>";
    foreach ($_REQUEST['dataIndicator'] as $dataIndicator) {
      $chartTitle = $dataIndicator;
      //$chartTitle = str_replace('_', ' ', $dataIndicator);
      //$chartTitle = str_replace('no', 'No ', $chartTitle);
      $popupContent.= "<tr><td>" . $chartTitle . "</td>";
      $popupContent.= "<td><b>&nbsp;&nbsp;" . intval($data[$dataIndicator]) . "</b></td></tr>";
    }
    $popupContent.="</table>";
    $coord = getDistrictCordinate($data['name_3']);
    echo "L.marker([" . $coord['lat'] . ", " . $coord['long'] . "]).addTo(map).bindPopup(\"$popupContent\").openPopup();
            ";
    }
    ?>

  </script>
</div>