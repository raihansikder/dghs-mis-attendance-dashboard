<h3> <span class="glyphicon glyphicon-list-alt"></span> TABULAR DATA</h3>
<table id="datatable_tableView" width="100%">
  <thead>
    <tr>
      <th>District</th>
      <?php
      foreach ($_REQUEST['dataIndicator'] as $dataIndicator) {
        $chartTitle = ucfirst(str_replace('_', ' ', $dataIndicator));
        echo "<th>" . $chartTitle . "</th>";
      }
      ?>

    </tr>
    <tr class="filterInput">
      <td><input type="text" name="District" value="" class="search_init" /></td>
        <?php
        foreach ($_REQUEST['dataIndicator'] as $dataIndicator) {
          echo "<td><input type=\"text\" name=\"$dataIndicator\" value=\"\" class=\"search_init\" /></td>";
        }
        ?>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($dataArray as $data) { ?>
      <tr>
        <td>
          <?= $data['name_3'] // name_3 = District    ?>
        </td>
        <?php
        foreach ($_REQUEST['dataIndicator'] as $dataIndicator) {
          echo "<td>" . intval($data[$dataIndicator]) . "</td>";
        }
        ?>

      </tr>
    <?php }
    ?>
  </tbody>
</table>

<script>
  oTable = $('#datatable_tableView').dataTable({
    "bPaginate": false,
    "sPaginationType": "full_numbers",
    "iDisplayLength": 25,
    "bStateSave": false,
    "oLanguage": {
      "sSearch": "Search all columns:"
    },
    "bSortCellsTop": true
  });

  /*Datatable sort*/
  $("thead input").keyup(function() {
    /* Filter on the column (the index) of this element */
    oTable.fnFilter(this.value, $("thead input").index(this));
    var index = $("thead input").index(this);
    index++;
    //alert(index);
    $("#datatable_tableView tbody tr td:nth-child(" + index + ")").removeHighlight();
    $("#datatable_tableView tbody tr td:nth-child(" + index + ")").highlight($(this).val());
  });
  /*
   * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
   * the footer
   */
  $("thead input").each(function(i) {
    //asInitVals[i] = this.value;
  });

  $("thead input").focus(function() {
    if (this.className == "search_init")
    {
      this.className = "search_init_focus";
      this.value = "";
    }
  });

  $("thead input").blur(function(i) {
    if (this.value == "")
    {
      this.className = "search_init";
      this.value = asInitVals[$("thead input").index(this)];
    }
  });


</script>