<h3><span class="glyphicon glyphicon-stats"></span> BAR CHART</h3>
<?php
$chartHeight = calculateChartHeight($districtCount, count($_REQUEST['dataIndicator']));
$FirstRowVal = "'District'";
$legend = array();
foreach ($_REQUEST['dataIndicator'] as $dataIndicator) {
  $FirstRowVal.= ", '" . ucfirst(str_replace('_', ' ', $dataIndicator)) . "'";
  array_push($legend, $dataIndicator);
}
?>

<div id="chart_div" style="width: 900px; height: <?= $chartHeight ?>px; position: relative"></div>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
  google.load("visualization", "1", {packages: ["corechart"]});
  google.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      [<?= $FirstRowVal ?>]
<?php
foreach ($dataArray as $data) {
  echo ",[";
  echo "'" . str_replace('District', '', $data['name_3']) . "'"; // remove 'District' from string
  foreach ($_REQUEST['dataIndicator'] as $dataIndicator) {
    if (strlen($data[$dataIndicator])) {
      echo "," . $data[$dataIndicator];
    } else {
      echo ", 0";
    }
  }
  echo "]";
}
?>

]);

var options = {
title: 'DGHS MIS - Data Analyzer',
chartArea: {left: 150, top: 0, width:"50%", height: "95%"},
vAxis: {title: '<?= $year ?>', titleTextStyle: {color: 'red'}}

};

var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
chart.draw(data, options);
}
</script>