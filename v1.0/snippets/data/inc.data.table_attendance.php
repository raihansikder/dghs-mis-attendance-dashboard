<h3> <span class="glyphicon glyphicon-list-alt"></span> <?=locale("attendence_details_table_header")?></h3>
<table id="datatable_tableView" width="100%">
    <thead>
        <tr>
            <th><?=locale("org_code")?></th>
            <th><?=locale("org_hrm_name")?></th>
            <th><?=locale("org_hrm_division_name")?></th>
            <th><?=locale("org_hrm_district_name")?></th>
            <th><?=locale("org_hrm_upazila_name")?></th>
            <th><?=locale("org_hrm_org_type_name")?></th>
            <th><?=locale("org_hrm_org_level_name")?></th>
            <th><?=locale("org_hrm_org_photo")?></th>
            <th><?=locale("org_hrm_land_phone1")?></th>
            <th><?=locale("org_hrm_mobile_number1")?></th>
            <th><?=locale("org_hrm_email_address1")?></th>
            <th><?=locale("totalEmployee")?></th>
            <th><?=locale("totalAttendance")?></th>
            <th><?=locale("machine_status")?></th>
        </tr>
        <tr class="filterInput">
            <td><input type="text" name="" value="" class="search_init" /></td>
            <td><input type="text" name="" value="" class="search_init" /></td>
            <td><input type="text" name="" value="" class="search_init" /></td>
            <td><input type="text" name="" value="" class="search_init" /></td>
            <td><input type="text" name="" value="" class="search_init" /></td>
            <td><input type="text" name="" value="" class="search_init" /></td>
            <td><input type="text" name="" value="" class="search_init" /></td>
            <td><input type="text" name="" value="" class="search_init" /></td>
            <td><input type="text" name="" value="" class="search_init" /></td>
            <td><input type="text" name="" value="" class="search_init" /></td>
            <td><input type="text" name="" value="" class="search_init" /></td>
            <td><input type="text" name="" value="" class="search_init" /></td>
            <td><input type="text" name="" value="" class="search_init" /></td>
            <td><input type="text" name="" value="" class="search_init" /></td>
        </tr>
    </thead>
    <tbody>
        <?php foreach($dataArray as $data){?>
        <tr>
            <td><?= $data['detail']['org_code'] ?><br/><span class="small"><i><?= $data['detail']['hosid'] ?></i></span></td>
            <td><?= $data['detail']['org_hrm_name'] ?></td>
            <td><?= $data['detail']['org_hrm_division_name'] ?></td>
            <td><?= $data['detail']['org_hrm_district_name'] ?></td>
            <td><?= $data['detail']['org_hrm_upazila_name'] ?></td>
            <td><?= $data['detail']['org_hrm_org_type_name'] ?></td>
            <td><?= $data['detail']['org_hrm_org_level_name'] ?></td>
            <td><a target="_blank" href="<?= $data['detail']['org_hrm_org_photo'] ?>">Photo</a></td>
            <td><?= $data['detail']['org_hrm_land_phone1'] ?></td>
            <td><?= $data['detail']['org_hrm_mobile_number1'] ?></td>
            <td><?= $data['detail']['org_hrm_email_address1'] ?></td>
            <td><?= $data['totalEmployee'] ?></td>
            <td><?= $data['totalAttendance'] ?></td>
            <td><?= $data['machine_status_details']['machine_status_name'] ?></td>
        </tr>
        <?php }?>

    </tbody>
</table>

<script>
    oTable = $('#datatable_tableView').dataTable({
        "bPaginate": true,
        "sPaginationType": "full_numbers",
        "iDisplayLength": 25,
        "bStateSave": false,
        "oLanguage": {
            "sSearch": "Search all columns:"
        },
        "bSortCellsTop": true
    });

    /*Datatable sort*/
    $("thead input").keyup(function() {
        /* Filter on the column (the index) of this element */
        oTable.fnFilter(this.value, $("thead input").index(this));
        var index = $("thead input").index(this);
        index++;
        //alert(index);
        $("#datatable_tableView tbody tr td:nth-child(" + index + ")").removeHighlight();
        $("#datatable_tableView tbody tr td:nth-child(" + index + ")").highlight($(this).val());
    });
    /*
     * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
     * the footer
     */
    $("thead input").each(function(i) {
        //asInitVals[i] = this.value;
    });

    $("thead input").focus(function() {
        if (this.className == "search_init")
        {
            this.className = "search_init_focus";
            this.value = "";
        }
    });

    $("thead input").blur(function(i) {
        if (this.value == "")
        {
            this.className = "search_init";
            this.value = asInitVals[$("thead input").index(this)];
        }
    });


</script>