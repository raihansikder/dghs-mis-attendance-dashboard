<form action="" method="get">
  <table id="tblFilter" class="tblFilter">
    <tr>
      <td>
        <b><span class="glyphicon glyphicon-calendar"></span> <span class="heading">YEAR </span></b><br/>
        <?php createSelectOptionsFrmArray($dataYears, $_REQUEST['dataYear'], 'dataYear', " class='validate[required]'"); ?>
      </td>
      <td>
        <b><span class="glyphicon glyphicon-calendar"></span><span class="heading"> MONTH </span></b><br/>
        <?php createSelectOptionsFrmArray($dataMonth, $_REQUEST['dataMonth'], 'dataMonth', " class=''"); ?>
      </td>
      <td>
        <b><span class="glyphicon glyphicon-globe"></span><span class="heading"> DIVISION</span></b><br/><?php createMultiSelectOptions('data_bd_division_2012', 'division_name', 'division_name', $customQuery, $datadivision_csv, 'datadivision[]', " class='multiselectdd'"); ?>
      </td>

      <td>
        <span style="padding-top: 8px; float: left; width: 196px;">
          <?php
          $button_color = "btn-primary";
          if (isset($_REQUEST['submit'])) {
            $button_color = "btn-warning";
          }
          ?>
          <button type="submit" name="submit" value="Filter" class=" btn <?= $button_color ?> btn-sm col-sm-6">ANALYZE</button>
          <a href="index.php" class='btn btn-link'>Reset</a>
        </span>
      </td>
    </tr>
  </table>
  <div class="clear"></div>
  <p>&nbsp;</p>
  <b><span class="glyphicon glyphicon-dashboard"></span><span class="heading"> DATA ELEMENT</span></b><br/><br/>
  <table>
    <tr>

      <td>
        <span class="heading">Disease Profile</span><br/><br/>
        <?php createMultiSelectOptions('data_indicator', 'indicator_name', 'indicator_readable_name', " WHERE indicator_group_name='disease_profile' ", $dataIndicator_csv, 'dataIndicator[]', " class='multiselectdd'"); ?></td>
      <td></td>
      <td>
        <span class="heading">EMOC</span><br/><br/>
        <?php createMultiSelectOptions('data_indicator', 'indicator_name', 'indicator_readable_name', " WHERE indicator_group_name='emoc' ", $dataIndicator_csv, 'dataIndicator[]', " class='multiselectdd'"); ?></td>
      <td></td>
      <td>
        <span class="heading">IMCI</span><br/><br/>
        <?php createMultiSelectOptions('data_indicator', 'indicator_name', 'indicator_readable_name', " WHERE indicator_group_name='imci' ", $dataIndicator_csv, 'dataIndicator[]', " class='multiselectdd'"); ?></td>
      <td></td>
      <td>
        <span class="heading">Epi Information</span><br/><br/>
        <?php createMultiSelectOptions('data_indicator', 'indicator_name', 'indicator_readable_name', " WHERE indicator_group_name='epi_information' ", $dataIndicator_csv, 'dataIndicator[]', " class='multiselectdd'"); ?></td>
      <td></td>
      <td>
        <span class="heading"> TT/MR Woman</span><br/><br/>
        <?php createMultiSelectOptions('data_indicator', 'indicator_name', 'indicator_readable_name', " WHERE indicator_group_name='epi_woman' ", $dataIndicator_csv, 'dataIndicator[]', " class='multiselectdd'"); ?></td>
      <td></td>
      <td>
        <span class="heading">Rabies Information</span><br/><br/>
        <?php createMultiSelectOptions('data_indicator', 'indicator_name', 'indicator_readable_name', " WHERE indicator_group_name='rabies_information' ", $dataIndicator_csv, 'dataIndicator[]', " class='multiselectdd'"); ?>
      </td>
      <td>
        <span class="heading">Community Clinic</span><br/><br/>
        <?php createMultiSelectOptions('data_indicator', 'indicator_name', 'indicator_readable_name', " WHERE indicator_group_name='community_clinic' ", $dataIndicator_csv, 'dataIndicator[]', " class='multiselectdd'"); ?>
      </td>
    </tr>
  </table>
  <?php ?>

</form>