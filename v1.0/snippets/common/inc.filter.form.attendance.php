<form action="" method="">
    <style type="text/css">
        .tblFilter tr td{ vertical-align: top; font-size: 9px;}
    </style>
    <table id="tblFilter" class="tblFilter" width="600px">
        <tr>
            <td>
                <b><span class="glyphicon glyphicon-calendar"></span><span class="heading"> DATE </span></b>
                <?php
                if(strlen(addEditInputField('date'))){
                    $date=addEditInputField('date');
                }
                ?>
                <input name="date" type="text" value="<?= $date ?>" size="20" style="float: none;" class="validate[required,custom[date]]" />
                <script>
                    $("input[name=date]").datepicker({
                        dateFormat: "yy-mm-dd",
                        changeMonth: true,
                        changeYear: true
                                //dateFormat: "dd-MM-yy",
                                //altField: "#sa_date_start_datetime",
                                //altFormat: "yy-mm-dd"
                    });
                    $("input[name=date]").change(function() {
                        $(this).validationEngine('validate')
                    });
                </script>
            </td>

            <td>
                <b><span class="glyphicon glyphicon-globe"></span><span class="heading"> FACILITY </span></b>
                <?php createMultiSelectOptions('hospitals', 'org_code', 'hosname', "WHERE org_code IS NOT NULL", $org_code_csv, 'org_code[]', " class='multiselectdd'"); ?>
                <input name="org_hrm_name" type="text" value="<?= addEditInputField('org_hrm_name') ?>" placeholder="Type Org name/code..." style="width: 150px;"/>
            </td>
            <td>
                <b><span class="glyphicon glyphicon-globe"></span><span class="heading"> DISTRICT </span></b>
                <?php createMultiSelectOptions('districts', 'disname', 'disname', "WHERE 1", $org_hrm_district_name_csv, 'org_hrm_district_name[]', " class='multiselectdd'"); ?>
            </td>
            <td>
                <b><span class="glyphicon glyphicon-globe"></span><span class="heading"> DIVISION </span></b>
                <?php createMultiSelectOptions('divisions', 'divname', 'divname', "WHERE 1", $org_hrm_division_name_csv, 'org_hrm_division_name[]', " class='multiselectdd'"); ?>
            </td>
            <td>
                <b><span class="glyphicon glyphicon-globe"></span><span class="heading"> MACHINE STATUS </span></b>
                <?php createMultiSelectOptions('machine_status', 'machine_status_id', 'machine_status_name', "WHERE 1", $machine_status_id_csv, 'machine_status_id[]', " class='multiselectdd'"); ?>
            </td>
            <td>

                <span style="padding-top: 8px; float: left; width: 300px;" class="btn-group">
                    <?php
                    $button_color = "btn-primary";
                    if (isset($_REQUEST['submit'])) {
                        $button_color = "btn-warning";
                    }
                    ?>
                    <button type="submit" name="submit" value="Filter" class=" btn <?php echo $button_color ?>"><span class="glyphicon glyphicon-ok-circle"></span> VIEW STATUS</button>
                    <a href="attendance.php" class="btn btn-default" >Reset</a>
                </span>
            </td>
        </tr>
    </table>
    <div class="clear"></div>
</form>