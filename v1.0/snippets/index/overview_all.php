<div id="filter">
  <h2>Overview</h2>
  <form action="" method="get">
    <table width="586" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td>
          Job number:<br/>
          <input name="job_id" type="text" value="<?php echo addEditInputField('job_id'); ?>" />
        </td>
        <td>
          Customer(s):<br/>
          <?php
          $selectedIdCsv = $job_customer_ids_csv;
          $customQuery = " WHERE customer_active='1' ";
          createMultiSelectOptions('customer', 'customer_id', 'customer_name', $customQuery, $selectedIdCsv, 'job_customer_id[]', " class='multiselectdd'");
          ?>
        </td>
        <td width="15%">
          Job name:<br/>
          <input class='autocomplete_jobname' name="job_name" value="<?php echo addEditInputField('job_name'); ?>" />
        </td>
        <td width="15%">
          Account Manager(s):<br/>
          <?php
          $selectedIdCsv = $account_manager_id_csv;
          $customQuery = " WHERE employee_active='1' AND employee_account_manager_option_value='Yes' ";
          createMultiSelectOptions('employee', 'employee_id', 'employee_first_name', $customQuery, $selectedIdCsv, 'account_manager_id[]', " class='multiselectdd'");
          ?>
        </td>
        <td width="15%">
          Partner(s):<br/>
          <?php
          $selectedIdCsv = $partner_id_csv;
          $customQuery = " WHERE partner_active='1' ";
          createMultiSelectOptions('partner', 'partner_id', 'partner_name', $customQuery, $selectedIdCsv, 'partner_id[]', " class='multiselectdd'");
          ?>
        </td>
        <td>
          <b>Required by</b> (start):<br/>
          <input name="job_delivery_date_start" type="text" value="<?php echo addEditInputField('job_delivery_date_start'); ?>" size="20" readonly="readonly" style="float: none;" />
          <input name="job_delivery_date_start_datetime" id="job_delivery_date_start_datetime" type="hidden" value="<?php echo addEditInputField('job_delivery_date_start_datetime'); ?>" size="20" class="" readonly="readonly" />
          <script>
            $("input[name=job_delivery_date_start]").datepicker({
              dateFormat: "dd-MM-yy",
              altField: "#job_delivery_date_start_datetime",
              altFormat: "yy-mm-dd"
            });
          </script>
        </td>
        <td>
          <b>Required by</b> (End):<br/>
          <input name="job_delivery_date_end" type="text" value="<?php echo addEditInputField('job_delivery_date_end'); ?>" size="20" class="" readonly="readonly" style="float: none;" />
          <input name="job_delivery_date_end_datetime" id="job_delivery_date_end_datetime" type="hidden" value="<?php echo addEditInputField('job_delivery_date_end_datetime'); ?>" size="20" class="" readonly="readonly" />
          <script>
            $("input[name=job_delivery_date_end]").datepicker({
              dateFormat: "dd-MM-yy",
              altField: "#job_delivery_date_end_datetime",
              altFormat: "yy-mm-dd"
            });
          </script>
        </td>
      </tr>
      <tr>
        <td>
          Status(s):<br/>
          <?php
          $selectedIdCsv = $job_current_status_csv;
          $customQuery = " WHERE option_group='job_status' AND option_active='1' ";
          createMultiSelectOptions('options', 'option_value', 'option_name', $customQuery, $selectedIdCsv, 'job_current_status[]', " class='multiselectdd'");
          ?>
        </td>
        <td>
          Approve Script Priority:<br/>
          <?php
          $selectedIdCsv = $job_approve_script_priority_csv;
          $customQuery = " WHERE option_group='priority' AND option_active='1' ";
          createMultiSelectOptions('options', 'option_value', 'option_name', $customQuery, $selectedIdCsv, 'job_approve_script_priority[]', " class='multiselectdd'");
          ?>
        </td>
        <td>
          Voice Artist Priority:<br/>
          <?php
          $selectedIdCsv = $job_voice_artist_priority_csv;
          $customQuery = " WHERE option_group='priority' AND option_active='1' ";
          createMultiSelectOptions('options', 'option_value', 'option_name', $customQuery, $selectedIdCsv, 'job_voice_artist_priority[]', " class='multiselectdd'");
          ?>
        </td>
        <td>
          Studio Priority:<br/>
          <?php
          $selectedIdCsv = $job_studio_priority_csv;
          $customQuery = " WHERE option_group='priority' AND option_active='1' ";
          createMultiSelectOptions('options', 'option_value', 'option_name', $customQuery, $selectedIdCsv, 'job_studio_priority[]', " class='multiselectdd'");
          ?>
        </td>
        <td>
          QA/Dispatch Priority:<br/>
          <?php
          $selectedIdCsv = $job_qa_dispatch_priority_csv;
          $customQuery = " WHERE option_group='priority' AND option_active='1' ";
          createMultiSelectOptions('options', 'option_value', 'option_name', $customQuery, $selectedIdCsv, 'job_qa_dispatch_priority[]', " class='multiselectdd'");
          ?>
        </td>
        <td>
          <b>Order number:</b><br/>
          <input name="job_order_id" type="text" value="<?php echo addEditInputField('job_order_id'); ?>" />
        </td>
        <td>
          <!--
        <b>Paragraph text:</b><br/>
        <input name="job_paragraph_text" type="text" />
          -->
          <span style="padding-top: 8px; float: left; width: 196px;">
            <?php
            $button_color = "btn-primary";
            if (isset($_REQUEST['submit'])) {
              $button_color = "btn-warning";
            }
            ?>
            <button type="submit" name="submit" value="Filter" class=" btn <?= $button_color ?> btn-sm col-sm-6">FILTER</button>
            <a href="index.php" class='btn btn-link'>Reset</a>
          </span>
        </td>
      </tr>
    </table>
    <div class="clear"></div>

  </form>
</div>
<div class="clear"></div>
<table width="100%" cellspacing="1" id="overview_datatable">
  <thead>
    <tr>
      <th>Job<br/> Number</th>
      <th>Customer</th>
      <th>Job Name</th>
      <th>Account<br/> Manager</th>
      <th>Partner</th>
      <th>Required by</th>
      <th>Status</th>
      <th>Approve<br/> Script Priority</th>
      <th>Voice<br/> Artist Priority</th>
      <th>Studio<br/> Priority</th>
      <th>QC/Dispatch<br/> Priority</th>
    </tr>
    <tr class="filterInput">
      <td><input type="text" name="search_job_id" value="" class="search_init" /></td>
      <td><input type="text" name="search_customer_name" value="" class="search_init" /></td>
      <td><input type="text" name="search_job_name" value="" class="search_init" /></td>
      <td><input type="text" name="search_account_manager" value="" class="search_init" /></td>
      <td><input type="text" name="search_partner" value="" class="search_init" /></td>
      <td><input type="text" name="search_required_by" value="" class="search_init" /></td>
      <td><input type="text" name="search_jobCurrentStatus" value="" class="search_init" /></td>
      <td><input type="text" name="search_ApproveScriptPriority" value="" class="search_init" /></td>
      <td><input type="text" name="search_VoiceArtistPriority" value="" class="search_init" /></td>
      <td><input type="text" name="search_StudioPriority" value="" class="search_init" /></td>
      <td><input type="text" name="search_QAPriority" value="" class="search_init" /></td>
    </tr>
  </thead>
  <tbody>
    <?php
    if ($rows) {
      foreach ($jobs_array as $job) {
        $job_id = $job[job_id];
        $job_name = $job[job_name];
        $customer_name = getCustomerNameFrmId($job[job_customer_id]);
        $job_name = $job[job_name];
        $account_manager = getEmployeeNameFrmId(getAccoutManagerIdFrmCustomerId($job[job_customer_id]));
        $partner = getPartnerNameFrmId(getPartnerIdFromCustomerId(($job[job_customer_id])));
        $required_by = $job[job_delivery_date];
        //$jobCurrentStatus=jobCurrentStatus($job[job_id]);
        //saveJobPriority($job_id);
        $jobCurrentStatus = $job[job_current_status];
        $ApproveScriptPriority = printPriority($job[job_approve_script_priority]);
        $VoiceArtistPriority = printPriority($job[job_voice_artist_priority]);
        $StudioPriority = printPriority($job[job_studio_priority]);
        //$QAPriority=printPriority($job[job_qa_dispatch_priority]);
        $QADispatchPriority = printPriority($job[job_qa_dispatch_priority]);
        ?>
        <tr id="<?= $job_id ?>">
          <td><?= $job_id ?></td>
          <td><?= $customer_name ?></td>
          <td><a href="job_details.php?job_id=<?= $job_id ?>&param=edit" target="_blank"><?= $job_name ?></a></td>
          <td><?= $account_manager ?></td>
          <td><?= $partner ?></td>
          <td><?= $required_by ?></td>
          <td><?= $jobCurrentStatus ?></td>
          <td><?= $ApproveScriptPriority ?></td>
          <td><?= $VoiceArtistPriority ?></td>
          <td><?= $StudioPriority ?></td>
          <td><?= $QADispatchPriority ?></td>
        </tr>
        <?php
      }
    }
    ?>
  </tbody>
</table>
<script type="text/javascript" charset="utf-8">
  var asInitVals = new Array();
  $(document).ready(function() {
    oTable = $('#overview_datatable').dataTable( {
      "sPaginationType": "full_numbers",
      "iDisplayLength" : 25,
      "bStateSave": false,
      "oLanguage": {
        "sSearch": "Search all columns:"
      },
      "bSortCellsTop": true
    } );

    $("thead input").keyup( function () {
      /* Filter on the column (the index) of this element */
      oTable.fnFilter( this.value, $("thead input").index(this) );
      var index=$("thead input").index(this);
      index++;
      //alert(index);
      $("#overview_datatable tbody tr td:nth-child("+index+")").removeHighlight();
      $("#overview_datatable tbody tr td:nth-child("+index+")").highlight($(this).val());
    } );



    /*
     * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
     * the footer
     */
    $("thead input").each( function (i) {
      asInitVals[i] = this.value;
    } );
    /*
     * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
     * the footer
     */
    $("thead input").each( function (i) {
      asInitVals[i] = this.value;
    } );

    $("thead input").focus( function () {
      if ( this.className == "search_init" )
      {
        this.className = "";
        this.value = "";
      }
    } );

    $("thead input").blur( function (i) {
      if ( this.value == "" )
      {
        this.className = "search_init";
        this.value = asInitVals[$("thead input").index(this)];
      }
    } );

    $('title').html("Overview");

  } );
</script>