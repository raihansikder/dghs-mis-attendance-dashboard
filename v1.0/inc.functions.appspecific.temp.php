<?php
$emailtableStyle = " style='font-family:Arial, Helvetica, sans-serif;	font-size:12px;	'";

function emailCustomerAdd($customer_id) {
  global $mail;
  global $company;
  global $scriptpath;
  global $emailtableStyle;
  $customer = getRowVal('customer', 'customer_id', $customer_id);
  $account = getAccount($customer['customer_account_id']);
  $account_manager = getRowVal('employee', 'employee_id', $customer['customer_account_manager_id']);
  $mail->AddAddress($customer['customer_default_email'], $customer['customer_name']);
  $mail->Subject = "Welcome to " . $company['short_name'] . "'s Smart";
  $Body =
          "
	<table $emailtableStyle><tr><td>
    	Dear " . $customer['customer_default_contact_name'] . " <br><br>
    	As a customer of " . $company['long_name'] . ", we have given you a user log-on and password to access our web-based Studio Management And Recording Tool 'SMART'.  Using SMART you can request new recordings, track recording progress, download completed recordings and review historical information about your recordings.<br><br>
    	URL:		" . $scriptpath . "<br>
    	Username:  	" . $account['account_email'] . "<br>
    	Password: 	" . $account['account_password'] . "   (You will be prompted to change your password on your first log-in)<br><br>
    	Hopefully SMART is intuitive, but if you would like a brief training session, please just ask and I'd be happy to help.<br><br>
    	Yours<br><br>
    	" . $account_manager['employee_full_name'] . "<br>
    	" . $account_manager['employee_email'] . "<br>
    	" . $account_manager['employee_telephone'] . "<br><br>
    	" . $company['long_name'] . "<br>
    	08452 797 200<br>
	</td></tr></table>
	";

  $mail->Body = $Body;
  $mail->AltBody = str_replace('<br>', '\n\n', strip_tags($Body, '<br>'));
  if (!$mail->Send()) {
    echo "Mailer Error: " . $mail->ErrorInfo;
  }
  $mail->ClearAddresses();
}

function calculateChartHeight($orgunitCount, $indicatorCount) {
    $minChartHeight = 500;
    $height = $orgunitCount * $indicatorCount * 20;
    if ($height > 500) {
        return $height;
    }
    else
        return $minChartHeight;
}

function getDistrictCordinate($district_name) {
  if ($row = getRowVal("data_bd_district_coordinate", "district_name", $district_name)) {
    $arr = array();
    $arr['long'] = $row['long'];
    $arr['lat'] = $row['lat'];
    return $arr;
  }
}
?>

<?php

function drawPieChart($divName = 'piechart', $chartTitle = "My Pie Chart", $col1Title = 'Task', $col2Title = 'Hours per Day', $dataRows) {
  ?>


  <div id="<?= $divName ?>" style="width: 900px; height: auto;"></div>
  <script type="text/javascript">
    google.load("visualization", "1", {packages: ["corechart"]});
    google.setOnLoadCallback(drawChart);
    function drawChart() {
      var data = google.visualization.arrayToDataTable([
        ['<?= $col1Title ?>', '<?= $col2Title ?>']
  <?php
  foreach ($dataRows as $data) {
    echo ",['" . $data[0] . "',     " . $data[1] . "]";
  }
  ?>
      ]);
      var options = {
        title: '<?= $chartTitle ?>'
      };

      var chart = new google.visualization.PieChart(document.getElementById('<?= $divName ?>'));
      chart.draw(data, options);
    }
  </script>
  <?php
}
?>
<?php

function drawPieChartFromIndicator($divName = 'piechart', $chartTitle = "My Pie Chart", $col1Title = 'Task', $col2Title = 'Hours per Day', $dataArray, $indicator,$org_level_field_name='name_3') {
  ?>

  <script type="text/javascript">
    google.load("visualization", "1", {packages: ["corechart"]});
    google.setOnLoadCallback(drawChart);
    function drawChart() {
      var data = google.visualization.arrayToDataTable([
        ['<?= $col1Title ?>', '<?= $col2Title ?>']
  <?php
  foreach ($dataArray as $data) {
    echo ",['" . $data[$org_level_field_name] . "',     " . $data[$indicator] . "]";
    $chartDrawn = true;
  }
  ?>
      ]);
      var options = {
        title: '<?= $chartTitle ?>',
        is3D: true
      };

      var chart = new google.visualization.PieChart(document.getElementById('<?= $divName ?>'));
      chart.draw(data, options);
    }
  </script>
  <div id="<?= $divName ?>" style="width: 350px; height: 240px; float: left"></div>
  <?php
}
?>