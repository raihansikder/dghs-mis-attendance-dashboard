<?php

/**
 * This scirpt syncs with HRM API and stores the value in Local Attendance database.
 * @author John Doe <john.doe@example.com>
 * @copyright (c) 2013, Activation Ltd
 * @ticket #
 */
require_once '../config.php';

executeHospitalsSync();

/**
 * Main Sync function.
 * @author Enamul Haque <shuvoworld@gmail.com>
 * @copyright (c) 2013, Activation Ltd
 * @param none
 * @return bool true/false 
 */


function executeHospitalsSync() {    
    outputLog("Start Sync...");// print output     
    if ($hospitals = getAllHospitals()) { // get all hospitals
        outputLog("'".count($hospitals)."' Hospitals found");// print output log
        foreach ($hospitals as $hospital) { // iterate through each rows
            if($hrmOrgDetails=apiGetHrmOrgDetails($hospital['org_code'])){ // gets data from API
                //myprint_r($hrmOrgDetails); // prints what is got from API
                // store this data into 'hospitals' table field.
                // use the following setRowFieldVal() function to store values of one field at a time. and print a log every time a value is stored 
                // setRowFieldVal($dbTableName, $dbTargetFieldName, $dbTargetFieldValue, $UniqueIdFieldName, $UniqueIdValue)
                // outputLog("VALUE_XXX updated in hospitals.FIELDNAME ");// print output log
                foreach ($hrmOrgDetails as $hrmOrgDetail){
                setRowFieldVal('hospitals', "org_hrm_name", $hrmOrgDetail['org_name'], 'org_code', $hrmOrgDetail['org_code'] );
                outputLog("Value ". $hrmOrgDetail['org_name']." updated in hospitals.org_hrm_name");
                setRowFieldVal('hospitals', 'org_hrm_division_name', $hrmOrgDetail['division_name'], 'org_code', $hrmOrgDetail['org_code'] );
                outputLog("Value ". $hrmOrgDetail['division_name']." updated in hospitals.org_hrm_division_name");
                setRowFieldVal('hospitals', 'org_hrm_district_name', $hrmOrgDetail['district_name'], 'org_code', $hrmOrgDetail['org_code'] );
                outputLog("Value ". $hrmOrgDetail['district_name']." updated in hospitals.org_hrm_district_name");
                setRowFieldVal('hospitals', 'org_hrm_upazila_name', $hrmOrgDetail['upazila_thana_name'], 'org_code', $hrmOrgDetail['org_code'] );
                outputLog("Value ". $hrmOrgDetail['upazila_thana_name']." updated in hospitals.org_hrm_upazila_name");
                setRowFieldVal('hospitals', 'org_hrm_org_level_name', $hrmOrgDetail['org_level_name'], 'org_code', $hrmOrgDetail['org_code'] );
                outputLog("Value ". $hrmOrgDetail['org_level_name']." updated in hospitals.org_hrm_org_level_name");
                setRowFieldVal('hospitals', 'org_hrm_land_phone1', $hrmOrgDetail['land_phone1'], 'org_code', $hrmOrgDetail['org_code'] );
                outputLog("Value ". $hrmOrgDetail['land_phone1']." updated in hospitals.org_hrm_land_phone1");
                setRowFieldVal('hospitals', 'org_hrm_mobile_number1', $hrmOrgDetail['mobile_number1'], 'org_code', $hrmOrgDetail['org_code'] );
                outputLog("Value ". $hrmOrgDetail['mobile_number1']." updated in hospitals.org_hrm_mobile_number1");
                setRowFieldVal('hospitals', 'org_hrm_email_address1', $hrmOrgDetail['email_address1'], 'org_code', $hrmOrgDetail['org_code'] );
                outputLog("Value ". $hrmOrgDetail['email_address1']." updated in hospitals.org_hrm_email_address1");
                setRowFieldVal('hospitals', 'org_hrm_org_photo', "http://app.dghs.gov.bd/hrm/uploads/".$hrmOrgDetail['org_photo'], 'org_code', $hrmOrgDetail['org_code'] );
                outputLog("Value ". $hrmOrgDetail['org_photo']." updated in hospitals.org_hrm_org_photo");
                setRowFieldVal('hospitals', 'org_hrm_org_type_name', $hrmOrgDetail['org_type_code'], 'org_code', $hrmOrgDetail['org_code'] );
                outputLog("Value ". $hrmOrgDetail['org_type_code']." updated in hospitals.org_hrm_org_type_name");
                }
            }
            else{
                outputLog("Error: ".$hrmOrgDetail['org_code']." Couldn't get");// print output log
            }
              
          
        }
    }
}

/**
 * Get all Hospital rows in a 2-dimensional array using basic SQL query.
 * @author Enamul Haque <shuvoworld@gmail.com>
 * @copyright (c) 2013, Activation Ltd
 * @param none
 * @return ARRAY or FALSE(boolean)  
 */
//function getAllHospitals() {
    // returns array (use pre-written getRows() function) if results found
    // return FALSE if no entry found in 'hospitals' table. Which will no be the case here :)
	//$hospitals= getRows('hospitals');
	//return $hospitals;
//}

/**
 * Get org info from HRM using API and return as an array
 * @author Enamul Haque <shuvoworld@gmail.com>
 * @copyright (c) 2013, Activation Ltd
 * @param $org_code
 * @return ARRAY or FALSE(boolean)  
 */
function apiGetHrmOrgDetails($org_code){
    //returns array if results found
   // return FALSE if no entry found in 'hospitals' table. Which will no be the case here :)
$url = "http://app.dghs.gov.bd/hrm/api.php?org_code=".$org_code."&format=json";
$mydata = json_decode(file_get_contents($url), true);
return $mydata;
    
}

/**
 * echos a log line with timestamp at the front
 * @author Enamul Haque <shuvoworld@gmail.com>
 * @copyright (c) 2013, Activation Ltd
 * @param $string string
 * @return none
 */
function outputLog($string){
    echo "<b>" . getDateTime() . "</b> - $string<br/>"; // print output log
}
?>