<?php
include('config.php');
//echo $_SESSION['redirect_url'];
$valid = true;
$alert = array();
if (isset($_POST[Submit])) {
  $account_email = mysql_real_escape_string(trim($_POST[account_email]));
  $account_password = mysql_real_escape_string(trim($_POST[account_password]));

  if (empty($account_email) || empty($account_password)) {
    $valid = false;
    array_push($alert, "You cannot leave username or password field empty");
  }

  if ($valid) {
    $sql = "select * from account where account_email='$account_email' && account_password='$account_password'";
    $r = mysql_query($sql) or die(mysql_error());
    if (mysql_num_rows($r) > 0) {
      $a = mysql_fetch_assoc($r);
      /*
       *  load user info in SESSION
       */
      $_SESSION[current_user_id] = $a[account_id];
      $_SESSION[current_user_uid] = $a[account_uid];
      $_SESSION[current_user_type_name] = $a[account_user_type_name];
      $_SESSION[current_user_type_level] = getUserTypeLevel($a[account_user_type_name]);
      $_SESSION[current_user_email] = $a[account_email];
      $_SESSION[logged] = true;
      $_SESSION["logged_" . $a[account_uid]] = true;
      if ($a[account_user_type_name] != 'Superadmin') {
        $_SESSION[details] = getDetailsAccountInfo($a[account_id]);
      }
      $_SESSION[current_product_id] = $productId;
      /*       * ************************** */
      /*
       * update user login in database
       */
      insertLog('Login', 'User logged in', 'account', 'account_id', $_SESSION[current_user_id], "No Query String", $_SESSION[current_user_id], print_r($_SERVER, true));
    } else {
      $valid = false;
      array_push($alert, "username/password missmatch");
    }
  }
  if ($_SESSION[logged] == true) {
    if (firstLogin($_SESSION[current_user_id])) {
      header("location:my_account.php?first_login=1");
    } else {
      setRowFieldVal('account', 'account_last_login', getDateTime(), 'account_id', $_SESSION[current_user_id]);
      if (strlen($_SESSION['redirect_url'])) {
        header("location:" . $_SESSION['redirect_url']);
      } else {
        header("location:index.php");
      }
    }
  }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <?php include_once('inc.head.php') ?>
    <style>
      div#container {
        -moz-box-shadow: 3px 3px 5px 6px #ccc; -webkit-box-shadow: 3px 3px 5px 6px #ccc; box-shadow: 3px 3px 5px 6px #ccc;
      }
    </style>
  </head>
  <body>
    <div id="wrapper">
      <div id="container">
        <div id="top1">
          <?php include('top.php'); ?>
        </div>
        <div id="mid">
          <?php if (isset($_POST[Submit])) {
            printAlert($valid, $alert);
          } ?>
          <form action="" method="post" name="admin_login_form">
            <div style="float: left">
              <table>
                <tr>
                  <td width="50%">
                    <h2>User Login</h2>
                    <span style="float: left">Please input correct username and password to access the system.</span><br>
                      <table width="100%">
                        <tr>
                          <td width="86">&nbsp;</td>
                          <td width="465">&nbsp;</td>
                        </tr>
                        <tr>
                          <td>
                            <strong>User e-mail</strong>
                          </td>
                          <td>
                            <input type="text" name="account_email" class="validate[required]" />
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <strong>Password</strong>
                          </td>
                          <td>
                            <input type="password" name="account_password" class="validate[required]" />
                          </td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                          <td>
                            <input class="button bgblue" type="submit" name="Submit" value="Submit" />
                            <input class="button bgblue" type="reset" name="Reset" value="Reset" />
                          </td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                          <td>
                            <a href="forgot_password.php">Forgot Password</a> | <a href="<?= $helpSiteUrl; ?>" target="_blank">Help</a>
                          </td>
                        </tr>
                      </table>
                  </td>
                  <td>
                    <div class="news">
<?php include_once('snippets/common/news.php'); ?>
                    </div>
                  </td>
                </tr>
              </table>

            </div>
          </form>
        </div>
      </div>
      <div id="footer">
<?php include('footer.php'); ?>
      </div>
    </div>
  </body>
</html>
