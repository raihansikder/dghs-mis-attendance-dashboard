<?php
include_once("config.php");
$parameterized_query = " 1 ";
if (count($_REQUEST['dataDistrict'])) {
  $dataDistrict_csv = "'" . implode("','", $_REQUEST['dataDistrict']) . "'";
  $parameterized_query.=" AND name_3 in ($dataDistrict_csv)";
}
if (count($_REQUEST['dataIndicator'])) {
  $dataIndicator_csv = implode(',', $_REQUEST['dataIndicator']);
}

$year = $_REQUEST['dataYear'];
if (!strlen($year)) {
  $year = '2013';
}
$month = $_REQUEST['dataMonth'];
if (!strlen($month)) {
  $month = '';
}

$dbTableName = "data_bd_district_" . $year;
if (strlen($month)){
  $dbTableName = "data_bd_district_" . $year . "_m" . $month;
}

$q_filtered = "SELECT * FROM " . $dbTableName . " WHERE " . $parameterized_query;
$dataArray = getRowsFromQuery($q_filtered); // get all data


if (count($_REQUEST['dataDistrict'])) {
  $districtCount = count($_REQUEST['dataDistrict']);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <?php include_once('inc.head.php') ?>
    <style>
      th{width: 40px;}
    </style>
  </head>
  <body>
    <div id="wrapper">
      <div id="container">
        <?php include_once('top.php'); // static top menu       ?>
        <div id="mid">
          <div class="filterContainer">
            <?php include_once 'snippets/common/inc.filter.form.data_district.php'; ?>
          </div>
          <?php if (isset($_REQUEST['submit']) && $dataArray) : ?>
            <div class="clear"></div>
            <div class="">
              <ul class="nav nav-tabs nav-justified">
                <li class="active"><a href="#barChart" data-toggle="tab">BAR CHART</a></li>
                <li><a href="#dataTable" data-toggle="tab">TABULAR DATA</a></li>
                <li><a href="#pieChart" data-toggle="tab">PIE CHART</a></li>
                <li id="mapTabTitle"><a href="#mapTab" data-toggle="tab" id="mapTabAchor">LOCATION IN MAP</a></li>
              </ul>
              <!-- Tab panes -->
              <div class="tab-content">
                <div class="tab-pane fade in active" id="barChart">
                  <?php require_once 'snippets/data/inc.data.barchart_district.php'; ?>
                </div>
                <div class="tab-pane fade" id="pieChart">
                  <?php require_once 'snippets/data/inc.data.piechart_district.php'; ?>
                </div>
                <div class="tab-pane fade" id="dataTable">
                  <?php require_once 'snippets/data/inc.data.tabular_district.php'; ?>
                </div>
                <div class="tab-pane fade" id="mapTab">


                </div>
              </div>

            </div>
            <?php
          else:
            echo "<div class='alert'>No data found</div>";
            ?>
          <?php endif; ?>
          <div class="clear"></div>
          <?php require_once 'snippets/data/inc.data.map_district.php'; ?>
        </div>

        <div id="footer">
          <div class="clear"></div>
          <?php
          include('footer.php');
          ?>
        </div>

      </div>
    </div>
  </body>
</html>


<script type="text/javascript">



    $('#mainnav li#overview').addClass('active');

    </script>