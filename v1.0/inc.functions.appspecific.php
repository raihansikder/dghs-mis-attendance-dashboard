<?php

/**
 * overrides inc.globalvariables.php
 *
 */
/* * ******************************** */
/*
 * 	file upload parameters
 */
$max_upload_img_size = 10000000; // maximum image upload size in kb
$img_upload_types = array("image/jpeg", "image/gif", "image/pjpeg", "image/jpg", "image/png");
/* * ******************************** */
$month_arr = array('month', 'jan', 'feb', 'mar', 'apr', 'may', 'jun', 'july', 'aug', 'sep', 'oct', 'nov', 'dec');
$defaultConfirmationMsg = "Please confirm the action by checking the box to the left.";
$SystemDefaultLogo = "images/logo.png";

$dataYears = array('2012', '2013'); // shows in filter dropdown menu
$dataMonth = array('1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12');

$districtCount = 64;
$divisionCount = 7;
$OrgRegistryPath="http://app.dghs.gov.bd/orgregistry/org_profile.php";

$indicator = array(
    "no_of_ANC_service_recipients",
    "no_of_admitted_women",
    "no_of_cesarean_section",
    "no_forceps_vacuum_destructive_operation",
    "no_of_live_birth_LB",
    "no_of_maternal_deaths_MD",
    "no_of_normal_delivery",
    "no_of_vaginal_deliveries_with_breech_of_face_presentation",
    "no_of_complication_4",
    "no_of_complication_2",
    "no_of_complication_8",
    "no_of_complication_12",
    "no_of_complication_13",
    "no_of_complication_11",
    "no_of_complication_3",
    "no_of_cases_with_pregnancy_delivery_related_complications",
    "no_of_complication_1",
    "no_of_complication_9",
    "no_of_complication_5",
    "no_of_complication_6",
    "no_of_complication_7",
    "no_of_complication_10",
    "no_of_other_pregnancy_related_surgeries",
    "no_of_patients_referred_out",
    "no_of_pregnant_woment_received_misoprostol_tablets",
    "no_of_total_neonatal_deaths",
    "no_of_women_received_post_natal_care_PNC",
    "no_of_normal_deliveries_with_misoprostol",
    "no_of_total_complicated_mother",
    "no_of_IMCI_cough_and_cold_no_pneumonia",
    "no_of_IMCI_dirrhea",
    "no_of_IMCI_ear_problem",
    "no_of_IMCI_female",
    "no_of_IMCI_fever_malaria",
    "no_of_IMCI_no_malaria",
    "no_of_IMCI_male",
    "no_of_IMCI_measles",
    "no_of_IMCI_other_disease",
    "no_of_IMCI_pneumonia",
    "no_of_IMCI_protein_and_energy_malnutrition_PEM",
    "no_of_IMCI_referred_child",
    "no_of_IMCI_very_severe_disease",
    "no_of_still_births",
    "no_of_normal_delivery_with_AMTSL_at_facility",
    "no_of_patients_referred_in"
);

$machineStatusList=array(
    "Allocated by not installed yet",
    "Installed and functioning",
    "Installed and functioning"
);

include_once('inc.functions.appspecific.temp.php');

function getUserNameFrmId($user_id) {
  $sql = "select user_first_name from user where user_id='$user_id'";
  $r = mysql_query($sql) or die(mysql_error() . "<b>Query:</b><br>___<br>$sql<br>");
  $a = mysql_fetch_assoc($r);
  //echo $a[user_first_name];
  return $a[user_first_name];
}

function getUserEmailFrmId($user_id) {
  $sql = "select account_email from account where account_id='$user_id'";
  $r = mysql_query($sql) or die(mysql_error() . "<b>Query:</b><br>___<br>$sql<br>");
  $a = mysql_fetch_assoc($r);
  //echo $a[user_email]."<br>";
  return $a[account_email];
}

function getUserPhoneFrmId($user_id) {
  $sql = "select user_phone from user where user_id='$user_id'";
  $r = mysql_query($sql) or die(mysql_error() . "<b>Query:</b><br>___<br>$sql<br>");
  $a = mysql_fetch_assoc($r);
  //echo $a[user_email]."<br>";
  return $a[user_phone];
}

function getClientCompanyNameFrmId($client_id) {
  $sql = "select client_company_name from client where client_id='$client_id'";
  $r = mysql_query($sql) or die(mysql_error() . "<b>Query:</b><br>___<br>$sql<br>");
  $a = mysql_fetch_assoc($r);
  return $a[client_company_name];
}

function getUserTypeLevel($user_type_name) {
  $sql = "select user_type_level from user_type where user_type_name='$user_type_name'";
  $r = mysql_query($sql) or die(mysql_error() . "<b>Query:</b><br>___<br>$sql<br>");
  $a = mysql_fetch_assoc($r);
  return $a['user_type_level'];
}

function getUserTypeName($user_type_id) {
  $sql = "select user_type_name from user_type where user_type_id='$user_type_id'";
  $r = mysql_query($sql) or die(mysql_error() . "<b>Query:</b><br>___<br>$sql<br>");
  $a = mysql_fetch_assoc($r);
  return $a['user_type_name'];
}

function getClientContactNameFrmId($client_id) {
  $sql = "select client_contact_name from client where client_id='$client_id'";
  $r = mysql_query($sql) or die(mysql_error() . "<b>Query:</b><br>___<br>$sql<br>");
  $a = mysql_fetch_assoc($r);
  return $a[client_contact_name];
}

function getClientEmailFrmId($client_id) {
  $sql = "select client_email from client where client_id='$client_id'";
  $r = mysql_query($sql) or die(mysql_error() . "<b>Query:</b><br>___<br>$sql<br>");
  $a = mysql_fetch_assoc($r);
  return $a[client_email];
}

function getCustomerNameFrmId($customer_id) {
  $sql = "select customer_name from customer where customer_id='$customer_id'";
  $r = mysql_query($sql) or die(mysql_error() . "<b>Query:</b><br>___<br>$sql<br>");
  $a = mysql_fetch_assoc($r);
  return $a[customer_name];
}

function getClientUserIds($client_id) {
  $sql = "select client_user_ids from client where client_id='$client_id'";
  $r = mysql_query($sql) or die(mysql_error() . "<b>Query:</b><br>___<br>$sql<br>");
  $a = mysql_fetch_assoc($r);
  return $a[client_user_ids];
}

/*
 * 	returns corresponding user_id if matching e-mail is found. else returns false;
 */

function emailIsAlreadyTaken($emailAddress) {
  $sql = "select account_uid from account where account_email='$emailAddress'";
  $r = mysql_query($sql) or die(mysql_error() . "<b>Query:</b><br>$sql<br>");
  if (mysql_num_rows($r)) {
    $a = mysql_fetch_assoc($r);
    //echo $a['account_uid'];
    return $a['account_uid'];
  }
  else
    return false;
}

function usernameIsAlreadyTaken($userName) {
  $sql = "select user_id from user where user_first_name='$userName'";
  $r = mysql_query($sql) or die(mysql_error() . "<b>Query:</b><br>___<br>$sql<br>");
  if (mysql_num_rows($r)) {
    $a = mysql_fetch_assoc($r);
    return $a['user_id'];
  }
  else
    return false;
}

function userTypeNameIsAlreadyTaken($userTypeName) {
  $sql = "select user_type_id from user_type where user_type_name='$userTypeName'";
  $r = mysql_query($sql) or die(mysql_error() . "<b>Query:</b><br>___<br>$sql<br>");
  if (mysql_num_rows($r)) {
    $a = mysql_fetch_assoc($r);
    return $a['user_type_id'];
  }
  else
    return false;
}

/*
 * 	Costsheet permission functions. CAREFULLY PUT MODULE NAME DONT TOUCH THE MODULE TABLE
 * 	returns true if the function has permission
 */

function hasPermission($module_system_name, $action, $user_id) {
  $p_user_types = userTypesPermittedForAction($module_system_name, $action);
  if (strlen($p_user_types)) {
    $sql = "select * from account where account_id='$user_id' and account_user_type_name in('" . str_replace(",", "','", trim($p_user_types, ",")) . "')";
    $r = mysql_query($sql) or die(mysql_error() . "<br>Query<br>_____<br>$sql<br>");
    if (mysql_num_rows($r))
      return true;
    else
      return false;
  }else {
    return false;
  }
}

/*
 *
 */

function addUserTypeIdInPermission($p_id, $new_user_type_id) {
  $existing_utids_csv = userTypeIdsFromPId($p_id);
  $existing_utids_array = explode(',', $existing_utids_csv);
  if (!in_array($new_user_type_id, $existing_utids_array)) {
    array_push($existing_utids_array, $new_user_type_id); // if not then value is added to array
    sort($existing_utids_array);
    $new_utids_csv = trim(implode(',', $existing_utids_array), ', ');
    $sql = "UPDATE permission
		SET p_user_type_ids='$new_utids_csv'
		Where p_id='$p_id'";
    $r = mysql_query($sql) or die(mysql_error() . "<b>Query:</b><br>___<br>$sql<br>");
  }
}

function removeUserTypeIdInPermission($p_id, $user_type_id_to_remove) {
  $existing_utids_csv = userTypeIdsFromPId($p_id);
  $existing_utids_array = explode(',', $existing_utids_csv);
  if (in_array($user_type_id_to_remove, $existing_utids_array)) {
    $new_utids_csv = trim(str_replace(",$user_type_id_to_remove,", "", ",$existing_utids_csv,"), ', ');
    $sql = "UPDATE permission
		SET p_user_type_ids='$new_utids_csv'
		Where p_id='$p_id'";
    $r = mysql_query($sql) or die(mysql_error() . "<b>Query:</b><br>___<br>$sql<br>");
  }
}

/* * ******************************************************** */
/*
 * 	Inserts uer_type_id in 'p_user_type_ids' field of each p_id
 *
 */

function updatePermissionTable($p_ids_array, $user_type_id) {
  $sql = "SELECT * FROM permission";
  $r = mysql_query($sql) or die(mysql_error() . "<b>Query:</b><br>___<br>$sql<br>");
  if (mysql_num_rows($r)) {
    $pa = mysql_fetch_rowsarr($r);
    foreach ($pa as $a) {
      if (count($p_ids_array) > 0) {
        if (in_array($a[p_id], $p_ids_array)) {
          addUserTypeIdInPermission($a[p_id], $user_type_id);
        } else {
          removeUserTypeIdInPermission($a[p_id], $user_type_id);
        }
      } else {
        removeUserTypeIdInPermission($a[p_id], $user_type_id);
      }
    }
  }
}

/* * ******************************************************** */
/*
 * 	returns a string of CSV values of user_type_ids matching 'module_system_name' and 'action'
 */

function userTypesPermittedForAction($module_system_name, $action) {
  $sql = "select p_user_type_names from permission where p_module_system_name='$module_system_name' and p_action='$action'";
  $r = mysql_query($sql) or die(mysql_error() . "<b>Query:</b><br>___<br>$sql<br>");
  $a = mysql_fetch_assoc($r);
  return trim($a['p_user_type_names'], ', ');
}

/* * ******************************************************** */
/*
 * 	returns a string of CSV values of user_type_ids
 */

function userTypeIdsFromPId($p_id) {
  $sql = "select p_user_type_ids from permission where p_id='$p_id'";
  $r = mysql_query($sql) or die(mysql_error() . "<b>Query:</b><br>___<br>$sql<br>");
  $a = mysql_fetch_assoc($r);
  return trim($a['p_user_type_ids'], ', ');
}

function currentUserLevel() {
  return $_SESSION[current_user_type_level];
}

function currentUserTypeId() {
  return $_SESSION[current_user_type_id];
}

function getOptNameFrmId($option_id) {
  $sql = "select option_name from options where option_id='$option_id' AND option_active='1'";
  $r = mysql_query($sql) or die(mysql_error() . "<b>Query:</b><br>$sql<br>");
  if (mysql_num_rows($r)) {
    $a = mysql_fetch_assoc($r);
    return $a['option_name'];
  } else {
    return false;
  }
}

function getActiveStatus($val) {
  if ($val == '1')
    echo "<span class='active'>Active</span>";
  else if ($val == '0')
    echo "<span class='inactive'>Inactive</span>";
}

function uploadImg($_FILES) {
  global $img_upload_types;

  if (in_array($_FILES["imagefile"]["type"], $img_upload_types) && (strlen($_FILES["imagefile"]["name"]))) {

    if ($_FILES["imagefile"]["error"] > 0) {
      echo "Return Code: " . $_FILES["imagefile"]["error"] . "<br/>";
    }
  else {
      $tmpName = $_FILES["imagefile"]["tmp_name"];
      $Name = $_FILES["imagefile"]["name"];
      $ext = substr(strrchr($Name, "."), 1);
      $newName = md5(rand() * time());
      $pic_save_address = "images/$newName.$ext";
      move_uploaded_file($_FILES["imagefile"]["tmp_name"], $pic_save_address);
      insertLog('General Module - Image Upload', 'New image uploaded', '', '', '', '', $_SESSION[current_user_id], "pic_save_address: $pic_save_address | " . print_r($_FILES, true) . print_r($_SERVER, true));
      return $pic_save_address;
    }
  } else {
    echo "Image Format Do not match.";
    return false;
  }
}

function uploadFile($_FILES, $inputName, $savedir = 'images', $typeArray = array('image/jpeg')) {
  if (in_array($_FILES[$inputName]["type"], $typeArray) && (strlen($_FILES[$inputName]["name"]))) {
    if ($_FILES["$inputName"]["error"] > 0) {
      echo "Return Code: " . $_FILES["$inputName"]["error"] . "<br/>";
    } else {
      $Name = $_FILES[$inputName]["name"];
      $ext = substr(strrchr($Name, "."), 1);
      $newName = md5(rand() * time());
      $file_save_address = "$savedir/$newName.$ext";
      move_uploaded_file($_FILES["$inputName"]["tmp_name"], $file_save_address);
      insertLog('General Module - Image Upload', 'New image uploaded', '', '', '', '', $_SESSION[current_user_id], "pic_save_address: $file_save_address | " . print_r($_FILES, true) . print_r($_SERVER, true));
      return $file_save_address;
    }
  } else {
    echo "type:" . $_FILES[$inputName]["type"];
    myprint_r($typeArray);
    echo "Format Do not match.";
    return false;
  }
}

function insertIntoAccountTable($account_uid, $account_email, $account_password, $account_user_type_name) {
  $account_user_type_name = ucfirst($account_user_type_name);
  $sql = "INSERT INTO account
	(account_uid,account_email,account_password,account_user_type_name)
	values
	('$account_uid','$account_email','$account_password','$account_user_type_name')";
  mysql_query($sql) or die(mysql_error() . "<b>Query:</b><br>$sql<br>");
  $account_id = mysql_insert_id();
  insertLog("Account Management-$account_user_type_name", "$account_user_type_name added", 'account', 'account_id', $account_id, $sql, $_SESSION[current_user_id], print_r($_SERVER, true));
  return $account_id;
}

function updateAccountTable($account_id, $account_email, $account_password, $account_user_type_name) {
  $account_user_type_name = ucfirst($account_user_type_name);
  $sql = "UPDATE account SET
	account_email='$account_email',
	account_password='$account_password',
	account_user_type_name='$account_user_type_name'
	WHERE account_id='$account_id'";
  mysql_query($sql) or die(mysql_error() . "<b>Query:</b><br>$sql<br>");
  insertLog("Account Management-$account_user_type_name", "$account_user_type_name updated", 'account', 'account_id', $account_id, $sql, $_SESSION[current_user_id], print_r($_SERVER, true));
}

function insertLog($log_module, $log_event, $log_affected_table_name, $log_affected_table_primary_key_field, $log_affected_table_primary_key_value, $log_sql_query_string, $log_event_user_id, $log_information) {
  $sql = "
      INSERT INTO log(
      log_module,
      log_event,
      log_affected_table_name,
      log_affected_table_primary_key_field,
      log_affected_table_primary_key_value,
      log_sql_query_string,
      log_event_user_id,
      log_datetime,
      log_information
      )value(
      '" . mysql_real_escape_string($log_module) . "',
      '" . mysql_real_escape_string($log_event) . "',
      '" . mysql_real_escape_string($log_affected_table_name) . "',
      '" . mysql_real_escape_string($log_affected_table_primary_key_field) . "',
      '" . mysql_real_escape_string($log_affected_table_primary_key_value) . "',
      '" . mysql_real_escape_string($log_sql_query_string) . "',
      '" . mysql_real_escape_string($log_event_user_id) . "',
      '" . getDateTime() . "',
      '" . mysql_real_escape_string($log_information) . "'
      )
      ";
  //echo $sql;
  $r = mysql_query($sql) or die(mysql_error() . "<br>Query:<br>____<br>$sql<br>");
}

function getAccountEmailFrmId($account_id) {
  return getRowFieldVal('account', 'account_email', 'account_id', $account_id);
}

function getHopitalDetails($hosid) {
  return getRowVal('hospitals', 'hosid', $hosid);
}

function getTotalEmployeeCount($hosid) {
  $result = getRowsFromQuery("SELECT COUNT(*) AS total FROM employees WHERE hosid='$hosid'"); // get all data
  return $result[0]['total'];
}

function getMachineStatusDetails($machine_status_id){
    return getRowVal('machine_status', 'machine_status_id', $machine_status_id);
}

function getTotalAttendanceCount($hosid, $date) {
  $result = getRowsFromQuery("  select count(distinct staffid) as total from logs WHERE hosid='$hosid' AND timing LIKE '%$date%';"); // get all data
  return $result[0]['total'];
}

function getAllHospitals() {
  return getRows('hospitals'," WHERE org_hrm_name IS NOT NULL");
}

?>
