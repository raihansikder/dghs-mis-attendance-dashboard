<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/jquery-ui.js" type="text/javascript"></script>


<!-- Bootstrap -->
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<script src="bootstrap/js/bootstrap.min.js"></script>

<!--
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
 -->
<link href="css/jquery-demos.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="js/themes/base/jquery.ui.all.css">

<!--Validation-->
<link href="css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
<script src="js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<!--Datatable-->
<link href="js/datatable/css/demo_table.css" media="screen" rel="stylesheet" type="text/css" />
<script src="js/datatable/js/jquery.dataTables.js" type="text/javascript"></script>
<!--JQuery UI core-->
<script src="js/ui/jquery.ui.core.js" type="text/javascript"></script>
<script src="js/ui/jquery.ui.widget.js" type="text/javascript"></script>
<script src="js/ui/jquery.ui.position.js" type="text/javascript"></script>
<script src="js/ui/jquery.ui.autocomplete.js" type="text/javascript"></script>
<script src="js/ui/jquery.ui.slider.js" type="text/javascript"></script>
<script src="js/ui/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
<!--Selectmenu-->
<script src="js/ui/jquery.ui.selectmenu.js" type="text/javascript"></script>
<link href="js/themes/base/jquery.ui.selectmenu.css" rel="stylesheet" type="text/css" />
<!-- Facebox script -->
<script src="facebox/facebox.js" type="text/javascript"></script>
<link href="facebox/facebox.css" media="screen" rel="stylesheet" type="text/css" />
<!-- -->
<!-- Multiselect script -->
<script src="js/multiselect/multiselectdd.js" type="text/javascript"></script>
<link href="js/multiselect/multiselectdd.css" media="screen" rel="stylesheet" type="text/css" />
<!-- -->
<script src="js/jquery.autogrow.js" type="text/javascript"></script>
<!-- Multiple select -->
<!-- <link type="text/css" href="multiple_select/css/ui.all.css" rel="stylesheet" /> -->
<link type="text/css" href="multiple_select/css/ui.multiselect.css" rel="stylesheet" />
<!--<script type="text/javascript" src="multiple_select/js/plugins/localisation/jquery.localisation-min.js"></script>
<script type="text/javascript" src="multiple_select/js/plugins/scrollTo/jquery.scrollTo-min.js"></script> -->
<script type="text/javascript" src="multiple_select/js/ui.multiselect.js"></script>
<!--Fancybox-->
<script type="text/javascript" src="fancybox/source/jquery.fancybox.js?v=2.1.3"></script>
<link rel="stylesheet" type="text/css" href="fancybox/source/jquery.fancybox.css?v=2.1.2" media="screen" />
<script type="text/javascript" src="js/jquery.highlight-4.js"></script>
<style>
.highlight { background-color: yellow }
</style>
<!-- Uploadify -->
<script src="uploadify/jquery.uploadify.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="uploadify/uploadify.css">
<!-- Map -->
 <link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.7/leaflet.css" />
    <script src="http://cdn.leafletjs.com/leaflet-0.7/leaflet.js"></script>

    <!--
    <script src="http://maps.google.com/maps/api/js?v=3.2&sensor=false"></script>
    <script src="http://matchingnotes.com/javascripts/leaflet-google.js"></script>
-->

<!-- Map -->
<link href="css/template_css.css" rel="stylesheet" type="text/css" />
