<?php
include_once("config.php");
$showtabs = false;
$parameterized_query = " org_hrm_name IS NOT NULL ";
if (count($_REQUEST['org_code'])) {
    $org_code_csv = "'" . implode("','", $_REQUEST['org_code']) . "'";
    $parameterized_query.=" AND org_code in ($org_code_csv)";
}
if (count($_REQUEST['org_hrm_district_name'])) {
    $org_hrm_district_name_csv = "'" . implode("','", $_REQUEST['org_hrm_district_name']) . "'";
    $parameterized_query.=" AND org_hrm_district_name in ($org_hrm_district_name_csv)";
}
if (count($_REQUEST['org_hrm_division_name'])) {
    $org_hrm_division_name_csv = "'" . implode("','", $_REQUEST['org_hrm_division_name']) . "'";
    $parameterized_query.=" AND org_hrm_division_name in ($org_hrm_division_name_csv)";
}
if(strlen($_REQUEST['org_hrm_name'])){
    $parameterized_query.=
            " AND (org_hrm_name LIKE '%".  mysql_real_escape_string(trim($_REQUEST['org_hrm_name']))."%' "
            . "OR org_code LIKE '%".  mysql_real_escape_string(trim($_REQUEST['org_hrm_name']))."%')";
}
if(strlen($_REQUEST['machine_status_id'])){
    $parameterized_query.=" AND machine_status LIKE '%".  mysql_real_escape_string(trim($_REQUEST['org_hrm_name']))."%'";
}

if (count($_REQUEST['machine_status_id'])) {
    $machine_status_id_csv = "'" . implode("','", $_REQUEST['machine_status_id']) . "'";
    $parameterized_query.=" AND machine_status in ($machine_status_id_csv)";
}

//org_hrm_district_name
//echo $_REQUEST['date'];
$date = date('Y-m-d');
if (strlen($_REQUEST['date'])) {
    $date = $_REQUEST['date'];
}


$dataArray = array();
/*
if (count($_REQUEST['hosid'])) {
    foreach ($_REQUEST['hosid'] as $hosid) {
        $dataArray[$hosid]['detail'] = getHopitalDetails($hosid);
        $dataArray[$hosid]['totalEmployee'] = getTotalEmployeeCount($hosid);
        $dataArray[$hosid]['totalAttendance'] = getTotalAttendanceCount($hosid, $date);
    }
} else {
    $hospitals = getAllHospitals();
    //myprint_r($hospitals);
}
 * 
 */



$q_filtered = "SELECT * FROM hospitals WHERE " . $parameterized_query;
$hospitals = getRowsFromQuery($q_filtered); // get all data
foreach ($hospitals as $hospital) {
    $hosid = $hospital['hosid'];
    $dataArray[$hosid]['detail'] = getHopitalDetails($hosid);
    $dataArray[$hosid]['machine_status_details'] = getMachineStatusDetails($hospital['machine_status']);
    $dataArray[$hosid]['totalEmployee'] = getTotalEmployeeCount($hosid);
    $dataArray[$hosid]['totalAttendance']=getTotalAttendanceCount($hosid, $date);
    if($dataArray[$hosid]['totalAttendance']>0){         
        $dataArray[$hosid]['machine_status_details']['machine_status_name'].= " - In Use";
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <?php include_once('inc.head.php') ?>
        <style>
            th{width: 40px;}
        </style>
    </head>
    <body>
        <div id="wrapper">
            <div id="container">
                <?php include_once('top.php'); // static top menu          ?>
                <div id="floater">
                    <div id="floaterIcons">
                        <div class="btn-group-vertical" style="color: #fff">
                            <a class="btn btn btn-danger" href="#filter_attendance" title="Jump to filter"><span class="glyphicon glyphicon-check"></span></a>
                            <a class="btn btn btn-warning" href="#map_attendance" title="Jump to map view"><span class="glyphicon glyphicon-globe"></span></a>
                            <a class="btn btn-primary" href="#table_attendance" title="Jump to table view"><span class="glyphicon glyphicon-list-alt"></span></a>
                        </div>
                    </div>
                </div>
                <div id="mid">
                    <div class="filterContainer">
                        <a id="filter_attendance"></a>
                        <?php include_once 'snippets/common/inc.filter.form.attendance.php'; ?>
                    </div>
                    <?php if (isset($_REQUEST['submit']) && $dataArray && $showtabs) : ?>
                        <div class="clear"></div>
                        <div class="">
                            <ul class="nav nav-tabs nav-justified">
                                <li class="active"><a href="#barChart" data-toggle="tab">BAR CHART</a></li>
                                <li><a href="#dataTable" data-toggle="tab">TABULAR DATA</a></li>
                                <li><a href="#pieChart" data-toggle="tab">PIE CHART</a></li>
                                <li id="mapTabTitle"><a href="#mapTab" data-toggle="tab" id="mapTabAchor">LOCATION IN MAP</a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="barChart">
                                    <?php require_once 'snippets/data/inc.data.barchart_district.php'; ?>
                                </div>
                                <div class="tab-pane fade" id="pieChart">
                                    <?php require_once 'snippets/data/inc.data.piechart_district.php'; ?>
                                </div>
                                <div class="tab-pane fade" id="dataTable">
                                    <?php require_once 'snippets/data/inc.data.tabular_district.php'; ?>
                                </div>
                                <div class="tab-pane fade" id="mapTab">


                                </div>
                            </div>

                        </div>
                        <?php
                    else:
                        //echo "<div class='clear'></div><div class='alert'>No data found</div>";
                        ?>
                    <?php endif; ?>
                    <div class="clear"></div>
                    <a id="map_attendance"></a>
                    <?php require_once 'snippets/data/inc.data.map_attendance.php'; ?>

                    <div class="clear"></div>
                    <a id="table_attendance"></a>
                    <?php require_once 'snippets/data/inc.data.table_attendance.php'; ?>
                </div>

                <div id="footer">
                    <?php
                    //myprint_r($dataArray);
                    ?>
                    <div class="clear"></div>
                    <?php
                    include('footer.php');
                    ?>
                </div>


            </div>
        </div>

    </body>
</html>