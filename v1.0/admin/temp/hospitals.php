<?php session_start();
  if (isset($_GET["order"])) $order = @$_GET["order"];
  if (isset($_GET["type"])) $ordtype = @$_GET["type"];

  if (isset($_POST["filter"])) $filter = @$_POST["filter"];
  if (isset($_POST["filter_field"])) $filterfield = @$_POST["filter_field"];
  $wholeonly = false;
  if (isset($_POST["wholeonly"])) $wholeonly = @$_POST["wholeonly"];

  if (!isset($order) && isset($_SESSION["order"])) $order = $_SESSION["order"];
  if (!isset($ordtype) && isset($_SESSION["type"])) $ordtype = $_SESSION["type"];
  if (!isset($filter) && isset($_SESSION["filter"])) $filter = $_SESSION["filter"];
  if (!isset($filterfield) && isset($_SESSION["filter_field"])) $filterfield = $_SESSION["filter_field"];

?>

<html>
<head>
<title>stagedgh_attendance -- hospitals</title>
<meta name="generator" http-equiv="content-type" content="text/html">
<style type="text/css">
  body {
    background-color: #FFFFFF;
    color: #004080;
    font-family: Arial;
    font-size: 12px;
  }
  .bd {
    background-color: #FFFFFF;
    color: #004080;
    font-family: Arial;
    font-size: 12px;
  }
  .tbl {
    background-color: #FFFFFF;
  }
  a:link { 
    background-color: #FFFFFF01;
    color: #FF0000;
    font-family: Arial;
    font-size: 12px;
  }
  a:active { 
    color: #0000FF;
    font-family: Arial;
    font-size: 12px;
  }
  a:visited { 
    color: #800080;
    font-family: Arial;
    font-size: 12px;
  }
  .hr {
    background-color: #336699;
    color: #FFFFFF;
    font-family: Arial;
    font-size: 12px;
  }
  a.hr:link {
    color: #FFFFFF;
    font-family: Arial;
    font-size: 12px;
  }
  a.hr:active {
    color: #FFFFFF;
    font-family: Arial;
    font-size: 12px;
  }
  a.hr:visited {
    color: #FFFFFF;
    font-family: Arial;
    font-size: 12px;
  }
  .dr {
    background-color: #FFFFFF;
    color: #000000;
    font-family: Arial;
    font-size: 12px;
  }
  .sr {
    background-color: #FFFFCF;
    color: #000000;
    font-family: Arial;
    font-size: 12px;
  }
</style>
</head>
<body>

<table width="100%">
<tr>

<td width="10%" valign="top">
<li><a href="districts.php?a=reset">districts</a>
<li><a href="divisions.php?a=reset">divisions</a>
<li><a href="employees.php?a=reset">employees</a>
<li><a href="machine_status.php?a=reset">machine_status</a>
</td>
<td width="5%">
</td>
<td bgcolor="#e0e0e0">
</td>
<td width="5%">
</td>
<td width="80%" valign="top">
<?php
  if (!login()) exit;
?>
<div style="float: right"><a href="hospitals.php?a=logout">[ Logout ]</a></div>
<br>
<?php
  $conn = connect();
  $showrecs = 20;
  $pagerange = 10;

  $a = @$_GET["a"];
  $recid = @$_GET["recid"];
  $page = @$_GET["page"];
  if (!isset($page)) $page = 1;

  $sql = @$_POST["sql"];

  switch ($sql) {
    case "insert":
      sql_insert();
      break;
    case "update":
      sql_update();
      break;
  }

  switch ($a) {
    case "add":
      addrec();
      break;
    case "view":
      viewrec($recid);
      break;
    case "edit":
      editrec($recid);
      break;
    default:
      select();
      break;
  }

  if (isset($order)) $_SESSION["order"] = $order;
  if (isset($ordtype)) $_SESSION["type"] = $ordtype;
  if (isset($filter)) $_SESSION["filter"] = $filter;
  if (isset($filterfield)) $_SESSION["filter_field"] = $filterfield;
  if (isset($wholeonly)) $_SESSION["wholeonly"] = $wholeonly;

  mysql_close($conn);
?>
</td></tr></table>

</body>
</html>

<?php function select()
  {
  global $a;
  global $showrecs;
  global $page;
  global $filter;
  global $filterfield;
  global $wholeonly;
  global $order;
  global $ordtype;


  if ($a == "reset") {
    $filter = "";
    $filterfield = "";
    $wholeonly = "";
    $order = "";
    $ordtype = "";
  }

  $checkstr = "";
  if ($wholeonly) $checkstr = " checked";
  if ($ordtype == "asc") { $ordtypestr = "desc"; } else { $ordtypestr = "asc"; }
  $res = sql_select();
  $count = sql_getrecordcount();
  if ($count % $showrecs != 0) {
    $pagecount = intval($count / $showrecs) + 1;
  }
  else {
    $pagecount = intval($count / $showrecs);
  }
  $startrec = $showrecs * ($page - 1);
  if ($startrec < $count) {mysql_data_seek($res, $startrec);}
  $reccount = min($showrecs * $page, $count);
?>
<table class="bd" border="0" cellspacing="1" cellpadding="4">
<tr><td>Table: hospitals</td></tr>
<tr><td>Records shown <?php echo $startrec + 1 ?> - <?php echo $reccount ?> of <?php echo $count ?></td></tr>
</table>
<hr size="1" noshade>
<form action="hospitals.php" method="post">
<table class="bd" border="0" cellspacing="1" cellpadding="4">
<tr>
<td><b>Custom Filter</b>&nbsp;</td>
<td><input type="text" name="filter" value="<?php echo $filter ?>"></td>
<td><select name="filter_field">
<option value="">All Fields</option>
<option value="<?php echo "upoid" ?>"<?php if ($filterfield == "upoid") { echo "selected"; } ?>><?php echo htmlspecialchars("upoid") ?></option>
<option value="<?php echo "hosid" ?>"<?php if ($filterfield == "hosid") { echo "selected"; } ?>><?php echo htmlspecialchars("hosid") ?></option>
<option value="<?php echo "hosname" ?>"<?php if ($filterfield == "hosname") { echo "selected"; } ?>><?php echo htmlspecialchars("hosname") ?></option>
<option value="<?php echo "status" ?>"<?php if ($filterfield == "status") { echo "selected"; } ?>><?php echo htmlspecialchars("status") ?></option>
<option value="<?php echo "long" ?>"<?php if ($filterfield == "long") { echo "selected"; } ?>><?php echo htmlspecialchars("long") ?></option>
<option value="<?php echo "lat" ?>"<?php if ($filterfield == "lat") { echo "selected"; } ?>><?php echo htmlspecialchars("lat") ?></option>
<option value="<?php echo "org_code" ?>"<?php if ($filterfield == "org_code") { echo "selected"; } ?>><?php echo htmlspecialchars("org_code") ?></option>
<option value="<?php echo "lp_machine_status" ?>"<?php if ($filterfield == "lp_machine_status") { echo "selected"; } ?>><?php echo htmlspecialchars("machine_status") ?></option>
<option value="<?php echo "org_hrm_name" ?>"<?php if ($filterfield == "org_hrm_name") { echo "selected"; } ?>><?php echo htmlspecialchars("org_hrm_name") ?></option>
<option value="<?php echo "org_hrm_division_name" ?>"<?php if ($filterfield == "org_hrm_division_name") { echo "selected"; } ?>><?php echo htmlspecialchars("org_hrm_division_name") ?></option>
<option value="<?php echo "org_hrm_district_name" ?>"<?php if ($filterfield == "org_hrm_district_name") { echo "selected"; } ?>><?php echo htmlspecialchars("org_hrm_district_name") ?></option>
<option value="<?php echo "org_hrm_upazila_name" ?>"<?php if ($filterfield == "org_hrm_upazila_name") { echo "selected"; } ?>><?php echo htmlspecialchars("org_hrm_upazila_name") ?></option>
<option value="<?php echo "org_hrm_org_type_name" ?>"<?php if ($filterfield == "org_hrm_org_type_name") { echo "selected"; } ?>><?php echo htmlspecialchars("org_hrm_org_type_name") ?></option>
<option value="<?php echo "org_hrm_org_level_name" ?>"<?php if ($filterfield == "org_hrm_org_level_name") { echo "selected"; } ?>><?php echo htmlspecialchars("org_hrm_org_level_name") ?></option>
<option value="<?php echo "org_hrm_org_photo" ?>"<?php if ($filterfield == "org_hrm_org_photo") { echo "selected"; } ?>><?php echo htmlspecialchars("org_hrm_org_photo") ?></option>
<option value="<?php echo "org_hrm_land_phone1" ?>"<?php if ($filterfield == "org_hrm_land_phone1") { echo "selected"; } ?>><?php echo htmlspecialchars("org_hrm_land_phone1") ?></option>
<option value="<?php echo "org_hrm_mobile_number1" ?>"<?php if ($filterfield == "org_hrm_mobile_number1") { echo "selected"; } ?>><?php echo htmlspecialchars("org_hrm_mobile_number1") ?></option>
<option value="<?php echo "org_hrm_email_address1" ?>"<?php if ($filterfield == "org_hrm_email_address1") { echo "selected"; } ?>><?php echo htmlspecialchars("org_hrm_email_address1") ?></option>
</select></td>
<td><input type="checkbox" name="wholeonly"<?php echo $checkstr ?>>Whole words only</td>
</td></tr>
<tr>
<td>&nbsp;</td>
<td><input type="submit" name="action" value="Apply Filter"></td>
<td><a href="hospitals.php?a=reset">Reset Filter</a></td>
</tr>
</table>
</form>
<hr size="1" noshade>
<?php showpagenav($page, $pagecount); ?>
<br>
<table class="tbl" border="0" cellspacing="1" cellpadding="5"width="100%">
<tr>
<td class="hr">&nbsp;</td>
<td class="hr">&nbsp;</td>
<td class="hr"><a class="hr" href="hospitals.php?order=<?php echo "upoid" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("upoid") ?></a></td>
<td class="hr"><a class="hr" href="hospitals.php?order=<?php echo "hosid" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("hosid") ?></a></td>
<td class="hr"><a class="hr" href="hospitals.php?order=<?php echo "hosname" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("hosname") ?></a></td>
<td class="hr"><a class="hr" href="hospitals.php?order=<?php echo "status" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("status") ?></a></td>
<td class="hr"><a class="hr" href="hospitals.php?order=<?php echo "long" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("long") ?></a></td>
<td class="hr"><a class="hr" href="hospitals.php?order=<?php echo "lat" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("lat") ?></a></td>
<td class="hr"><a class="hr" href="hospitals.php?order=<?php echo "org_code" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("org_code") ?></a></td>
<td class="hr"><a class="hr" href="hospitals.php?order=<?php echo "lp_machine_status" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("machine_status") ?></a></td>
<td class="hr"><a class="hr" href="hospitals.php?order=<?php echo "org_hrm_name" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("org_hrm_name") ?></a></td>
<td class="hr"><a class="hr" href="hospitals.php?order=<?php echo "org_hrm_division_name" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("org_hrm_division_name") ?></a></td>
<td class="hr"><a class="hr" href="hospitals.php?order=<?php echo "org_hrm_district_name" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("org_hrm_district_name") ?></a></td>
<td class="hr"><a class="hr" href="hospitals.php?order=<?php echo "org_hrm_upazila_name" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("org_hrm_upazila_name") ?></a></td>
<td class="hr"><a class="hr" href="hospitals.php?order=<?php echo "org_hrm_org_type_name" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("org_hrm_org_type_name") ?></a></td>
<td class="hr"><a class="hr" href="hospitals.php?order=<?php echo "org_hrm_org_level_name" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("org_hrm_org_level_name") ?></a></td>
<td class="hr"><a class="hr" href="hospitals.php?order=<?php echo "org_hrm_org_photo" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("org_hrm_org_photo") ?></a></td>
<td class="hr"><a class="hr" href="hospitals.php?order=<?php echo "org_hrm_land_phone1" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("org_hrm_land_phone1") ?></a></td>
<td class="hr"><a class="hr" href="hospitals.php?order=<?php echo "org_hrm_mobile_number1" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("org_hrm_mobile_number1") ?></a></td>
<td class="hr"><a class="hr" href="hospitals.php?order=<?php echo "org_hrm_email_address1" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("org_hrm_email_address1") ?></a></td>
</tr>
<?php
  for ($i = $startrec; $i < $reccount; $i++)
  {
    $row = mysql_fetch_assoc($res);
    $style = "dr";
    if ($i % 2 != 0) {
      $style = "sr";
    }
?>
<tr>
<td class="<?php echo $style ?>"><a href="hospitals.php?a=view&recid=<?php echo $i ?>">View</a></td>
<td class="<?php echo $style ?>"><a href="hospitals.php?a=edit&recid=<?php echo $i ?>">Edit</a></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["upoid"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["hosid"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["hosname"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["status"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["long"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["lat"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["org_code"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["lp_machine_status"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["org_hrm_name"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["org_hrm_division_name"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["org_hrm_district_name"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["org_hrm_upazila_name"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["org_hrm_org_type_name"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["org_hrm_org_level_name"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["org_hrm_org_photo"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["org_hrm_land_phone1"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["org_hrm_mobile_number1"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["org_hrm_email_address1"]) ?></td>
</tr>
<?php
  }
  mysql_free_result($res);
?>
</table>
<br>
<?php showpagenav($page, $pagecount); ?>
<?php } ?>

<?php function login()
{
  global $_POST;
  global $_SESSION;

  global $_GET;
  if (isset($_GET["a"]) && ($_GET["a"] == 'logout')) $_SESSION["logged_in"] = false;
  if (!isset($_SESSION["logged_in"])) $_SESSION["logged_in"] = false;
  if (!$_SESSION["logged_in"]) {
    $login = "";
    $password = "";
    if (isset($_POST["login"])) $login = @$_POST["login"];
    if (isset($_POST["password"])) $password = @$_POST["password"];

    if (($login != "") && ($password != "")) {
      $conn = mysql_connect("localhost", "root", "");
      mysql_select_db("stagedgh_attendance");
      $sql = "select `account_password` from `account` where `account_email` = '" .$login ."'";
      $res = mysql_query($sql, $conn) or die(mysql_error());
      $row = mysql_fetch_assoc($res) or $row = array(0 => "");;
      if (isset($row)) reset($row);

      if (isset($password) && ($password == trim(current($row)))) {
        $_SESSION["logged_in"] = true;
    }
    else {
?>
<p><b><font color="-1">Sorry, the login/password combination you've entered is invalid</font></b></p>
<?php } } }if (isset($_SESSION["logged_in"]) && (!$_SESSION["logged_in"])) { ?>
<form action="hospitals.php" method="post">
<table class="bd" border="0" cellspacing="1" cellpadding="4">
<tr>
<td>Login</td>
<td><input type="text" name="login" value="<?php echo $login ?>"></td>
</tr>
<tr>
<td>Password</td>
<td><input type="password" name="password" value="<?php echo $password ?>"></td>
</tr>
<tr>
<td><input type="submit" name="action" value="Login"></td>
</tr>
</table>
</form>
<?php
  }
  if (!isset($_SESSION["logged_in"])) $_SESSION["logged_in"] = false;
  return $_SESSION["logged_in"];
} ?>

<?php function showrow($row, $recid)
  {
?>
<table class="tbl" border="0" cellspacing="1" cellpadding="5"width="50%">
<tr>
<td class="hr"><?php echo htmlspecialchars("upoid")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["upoid"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("hosid")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["hosid"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("hosname")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["hosname"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("status")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["status"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("long")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["long"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("lat")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["lat"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("org_code")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["org_code"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("machine_status")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["lp_machine_status"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("org_hrm_name")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["org_hrm_name"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("org_hrm_division_name")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["org_hrm_division_name"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("org_hrm_district_name")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["org_hrm_district_name"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("org_hrm_upazila_name")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["org_hrm_upazila_name"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("org_hrm_org_type_name")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["org_hrm_org_type_name"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("org_hrm_org_level_name")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["org_hrm_org_level_name"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("org_hrm_org_photo")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["org_hrm_org_photo"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("org_hrm_land_phone1")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["org_hrm_land_phone1"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("org_hrm_mobile_number1")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["org_hrm_mobile_number1"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("org_hrm_email_address1")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["org_hrm_email_address1"]) ?></td>
</tr>
</table>
<?php } ?>

<?php function showroweditor($row, $iseditmode)
  {
  global $conn;
?>
<table class="tbl" border="0" cellspacing="1" cellpadding="5"width="50%">
<tr>
<td class="hr"><?php echo htmlspecialchars("hosname")."&nbsp;" ?></td>
<td class="dr"><textarea cols="35" rows="4" name="hosname"><?php echo str_replace('"', '&quot;', trim($row["hosname"])) ?></textarea></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("status")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="status" value="<?php echo str_replace('"', '&quot;', trim($row["status"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("long")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="long" maxlength="255" value="<?php echo str_replace('"', '&quot;', trim($row["long"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("lat")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="lat" value="<?php echo str_replace('"', '&quot;', trim($row["lat"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("org_code")."&nbsp;" ?></td>
<td class="dr"><textarea cols="35" rows="4" name="org_code"><?php echo str_replace('"', '&quot;', trim($row["org_code"])) ?></textarea></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("machine_status")."&nbsp;" ?></td>
<td class="dr"><select name="machine_status">
<option value=""></option>
<?php
  $sql = "select `machine_status_id`, `machine_status_name` from `machine_status`";
  $res = mysql_query($sql, $conn) or die(mysql_error());

  while ($lp_row = mysql_fetch_assoc($res)){
  $val = $lp_row["machine_status_id"];
  $caption = $lp_row["machine_status_name"];
  if ($row["machine_status"] == $val) {$selstr = " selected"; } else {$selstr = ""; }
 ?><option value="<?php echo $val ?>"<?php echo $selstr ?>><?php echo $caption ?></option>
<?php } ?></select>
</td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("org_hrm_name")."&nbsp;" ?></td>
<td class="dr"><textarea cols="35" rows="4" name="org_hrm_name" maxlength="100"><?php echo str_replace('"', '&quot;', trim($row["org_hrm_name"])) ?></textarea></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("org_hrm_division_name")."&nbsp;" ?></td>
<td class="dr"><textarea cols="35" rows="4" name="org_hrm_division_name"><?php echo str_replace('"', '&quot;', trim($row["org_hrm_division_name"])) ?></textarea></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("org_hrm_district_name")."&nbsp;" ?></td>
<td class="dr"><textarea cols="35" rows="4" name="org_hrm_district_name" maxlength="65535"><?php echo str_replace('"', '&quot;', trim($row["org_hrm_district_name"])) ?></textarea></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("org_hrm_upazila_name")."&nbsp;" ?></td>
<td class="dr"><textarea cols="35" rows="4" name="org_hrm_upazila_name" maxlength="255"><?php echo str_replace('"', '&quot;', trim($row["org_hrm_upazila_name"])) ?></textarea></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("org_hrm_org_type_name")."&nbsp;" ?></td>
<td class="dr"><textarea cols="35" rows="4" name="org_hrm_org_type_name" maxlength="255"><?php echo str_replace('"', '&quot;', trim($row["org_hrm_org_type_name"])) ?></textarea></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("org_hrm_org_level_name")."&nbsp;" ?></td>
<td class="dr"><textarea cols="35" rows="4" name="org_hrm_org_level_name" maxlength="255"><?php echo str_replace('"', '&quot;', trim($row["org_hrm_org_level_name"])) ?></textarea></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("org_hrm_org_photo")."&nbsp;" ?></td>
<td class="dr"><textarea cols="35" rows="4" name="org_hrm_org_photo" maxlength="255"><?php echo str_replace('"', '&quot;', trim($row["org_hrm_org_photo"])) ?></textarea></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("org_hrm_land_phone1")."&nbsp;" ?></td>
<td class="dr"><textarea cols="35" rows="4" name="org_hrm_land_phone1" maxlength="255"><?php echo str_replace('"', '&quot;', trim($row["org_hrm_land_phone1"])) ?></textarea></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("org_hrm_mobile_number1")."&nbsp;" ?></td>
<td class="dr"><textarea cols="35" rows="4" name="org_hrm_mobile_number1" maxlength="65535"><?php echo str_replace('"', '&quot;', trim($row["org_hrm_mobile_number1"])) ?></textarea></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("org_hrm_email_address1")."&nbsp;" ?></td>
<td class="dr"><textarea cols="35" rows="4" name="org_hrm_email_address1" maxlength="100"><?php echo str_replace('"', '&quot;', trim($row["org_hrm_email_address1"])) ?></textarea></td>
</tr>
</table>
<?php } ?>

<?php function showpagenav($page, $pagecount)
{
?>
<table class="bd" border="0" cellspacing="1" cellpadding="4">
<tr>
<td><a href="hospitals.php?a=add">Add Record</a>&nbsp;</td>
<?php if ($page > 1) { ?>
<td><a href="hospitals.php?page=<?php echo $page - 1 ?>">&lt;&lt;&nbsp;Prev</a>&nbsp;</td>
<?php } ?>
<?php
  global $pagerange;

  if ($pagecount > 1) {

  if ($pagecount % $pagerange != 0) {
    $rangecount = intval($pagecount / $pagerange) + 1;
  }
  else {
    $rangecount = intval($pagecount / $pagerange);
  }
  for ($i = 1; $i < $rangecount + 1; $i++) {
    $startpage = (($i - 1) * $pagerange) + 1;
    $count = min($i * $pagerange, $pagecount);

    if ((($page >= $startpage) && ($page <= ($i * $pagerange)))) {
      for ($j = $startpage; $j < $count + 1; $j++) {
        if ($j == $page) {
?>
<td><b><?php echo $j ?></b></td>
<?php } else { ?>
<td><a href="hospitals.php?page=<?php echo $j ?>"><?php echo $j ?></a></td>
<?php } } } else { ?>
<td><a href="hospitals.php?page=<?php echo $startpage ?>"><?php echo $startpage ."..." .$count ?></a></td>
<?php } } } ?>
<?php if ($page < $pagecount) { ?>
<td>&nbsp;<a href="hospitals.php?page=<?php echo $page + 1 ?>">Next&nbsp;&gt;&gt;</a>&nbsp;</td>
<?php } ?>
</tr>
</table>
<?php } ?>

<?php function showrecnav($a, $recid, $count)
{
?>
<table class="bd" border="0" cellspacing="1" cellpadding="4">
<tr>
<td><a href="hospitals.php">Index Page</a></td>
<?php if ($recid > 0) { ?>
<td><a href="hospitals.php?a=<?php echo $a ?>&recid=<?php echo $recid - 1 ?>">Prior Record</a></td>
<?php } if ($recid < $count - 1) { ?>
<td><a href="hospitals.php?a=<?php echo $a ?>&recid=<?php echo $recid + 1 ?>">Next Record</a></td>
<?php } ?>
</tr>
</table>
<hr size="1" noshade>
<?php } ?>

<?php function addrec()
{
?>
<table class="bd" border="0" cellspacing="1" cellpadding="4">
<tr>
<td><a href="hospitals.php">Index Page</a></td>
</tr>
</table>
<hr size="1" noshade>
<form enctype="multipart/form-data" action="hospitals.php" method="post">
<p><input type="hidden" name="sql" value="insert"></p>
<?php
$row = array(
  "upoid" => "",
  "hosid" => "",
  "hosname" => "",
  "status" => "",
  "long" => "",
  "lat" => "",
  "org_code" => "",
  "machine_status" => "",
  "org_hrm_name" => "",
  "org_hrm_division_name" => "",
  "org_hrm_district_name" => "",
  "org_hrm_upazila_name" => "",
  "org_hrm_org_type_name" => "",
  "org_hrm_org_level_name" => "",
  "org_hrm_org_photo" => "",
  "org_hrm_land_phone1" => "",
  "org_hrm_mobile_number1" => "",
  "org_hrm_email_address1" => "");
showroweditor($row, false);
?>
<p><input type="submit" name="action" value="Post"></p>
</form>
<?php } ?>

<?php function viewrec($recid)
{
  $res = sql_select();
  $count = sql_getrecordcount();
  mysql_data_seek($res, $recid);
  $row = mysql_fetch_assoc($res);
  showrecnav("view", $recid, $count);
?>
<br>
<?php showrow($row, $recid) ?>
<br>
<hr size="1" noshade>
<table class="bd" border="0" cellspacing="1" cellpadding="4">
<tr>
<td><a href="hospitals.php?a=add">Add Record</a></td>
<td><a href="hospitals.php?a=edit&recid=<?php echo $recid ?>">Edit Record</a></td>
</tr>
</table>
<?php
  mysql_free_result($res);
} ?>

<?php function editrec($recid)
{
  $res = sql_select();
  $count = sql_getrecordcount();
  mysql_data_seek($res, $recid);
  $row = mysql_fetch_assoc($res);
  showrecnav("edit", $recid, $count);
?>
<br>
<form enctype="multipart/form-data" action="hospitals.php" method="post">
<input type="hidden" name="sql" value="update">
<input type="hidden" name="xhosid" value="<?php echo $row["hosid"] ?>">
<?php showroweditor($row, true); ?>
<p><input type="submit" name="action" value="Post"></p>
</form>
<?php
  mysql_free_result($res);
} ?>

<?php function connect()
{
  $conn = mysql_connect("localhost", "root", "");
  mysql_select_db("stagedgh_attendance");
  return $conn;
}

function sqlvalue($val, $quote)
{
  if ($quote)
    $tmp = sqlstr($val);
  else
    $tmp = $val;
  if ($tmp == "")
    $tmp = "NULL";
  elseif ($quote)
    $tmp = "'".$tmp."'";
  return $tmp;
}

function sqlstr($val)
{
  return str_replace("'", "''", $val);
}

function sql_select()
{
  global $conn;
  global $order;
  global $ordtype;
  global $filter;
  global $filterfield;
  global $wholeonly;

  $filterstr = sqlstr($filter);
  if (!$wholeonly && isset($wholeonly) && $filterstr!='') $filterstr = "%" .$filterstr ."%";
  $sql = "SELECT * FROM (SELECT t1.`upoid`, t1.`hosid`, t1.`hosname`, t1.`status`, t1.`long`, t1.`lat`, t1.`org_code`, t1.`machine_status`, lp7.`machine_status_name` AS `lp_machine_status`, t1.`org_hrm_name`, t1.`org_hrm_division_name`, t1.`org_hrm_district_name`, t1.`org_hrm_upazila_name`, t1.`org_hrm_org_type_name`, t1.`org_hrm_org_level_name`, t1.`org_hrm_org_photo`, t1.`org_hrm_land_phone1`, t1.`org_hrm_mobile_number1`, t1.`org_hrm_email_address1` FROM `hospitals` AS t1 LEFT OUTER JOIN `machine_status` AS lp7 ON (t1.`machine_status` = lp7.`machine_status_id`)) subq";
  if (isset($filterstr) && $filterstr!='' && isset($filterfield) && $filterfield!='') {
    $sql .= " where " .sqlstr($filterfield) ." like '" .$filterstr ."'";
  } elseif (isset($filterstr) && $filterstr!='') {
    $sql .= " where (`upoid` like '" .$filterstr ."') or (`hosid` like '" .$filterstr ."') or (`hosname` like '" .$filterstr ."') or (`status` like '" .$filterstr ."') or (`long` like '" .$filterstr ."') or (`lat` like '" .$filterstr ."') or (`org_code` like '" .$filterstr ."') or (`lp_machine_status` like '" .$filterstr ."') or (`org_hrm_name` like '" .$filterstr ."') or (`org_hrm_division_name` like '" .$filterstr ."') or (`org_hrm_district_name` like '" .$filterstr ."') or (`org_hrm_upazila_name` like '" .$filterstr ."') or (`org_hrm_org_type_name` like '" .$filterstr ."') or (`org_hrm_org_level_name` like '" .$filterstr ."') or (`org_hrm_org_photo` like '" .$filterstr ."') or (`org_hrm_land_phone1` like '" .$filterstr ."') or (`org_hrm_mobile_number1` like '" .$filterstr ."') or (`org_hrm_email_address1` like '" .$filterstr ."')";
  }
  if (isset($order) && $order!='') $sql .= " order by `" .sqlstr($order) ."`";
  if (isset($ordtype) && $ordtype!='') $sql .= " " .sqlstr($ordtype);
  $res = mysql_query($sql, $conn) or die(mysql_error());
  return $res;
}

function sql_getrecordcount()
{
  global $conn;
  global $order;
  global $ordtype;
  global $filter;
  global $filterfield;
  global $wholeonly;

  $filterstr = sqlstr($filter);
  if (!$wholeonly && isset($wholeonly) && $filterstr!='') $filterstr = "%" .$filterstr ."%";
  $sql = "SELECT COUNT(*) FROM (SELECT t1.`upoid`, t1.`hosid`, t1.`hosname`, t1.`status`, t1.`long`, t1.`lat`, t1.`org_code`, t1.`machine_status`, lp7.`machine_status_name` AS `lp_machine_status`, t1.`org_hrm_name`, t1.`org_hrm_division_name`, t1.`org_hrm_district_name`, t1.`org_hrm_upazila_name`, t1.`org_hrm_org_type_name`, t1.`org_hrm_org_level_name`, t1.`org_hrm_org_photo`, t1.`org_hrm_land_phone1`, t1.`org_hrm_mobile_number1`, t1.`org_hrm_email_address1` FROM `hospitals` AS t1 LEFT OUTER JOIN `machine_status` AS lp7 ON (t1.`machine_status` = lp7.`machine_status_id`)) subq";
  if (isset($filterstr) && $filterstr!='' && isset($filterfield) && $filterfield!='') {
    $sql .= " where " .sqlstr($filterfield) ." like '" .$filterstr ."'";
  } elseif (isset($filterstr) && $filterstr!='') {
    $sql .= " where (`upoid` like '" .$filterstr ."') or (`hosid` like '" .$filterstr ."') or (`hosname` like '" .$filterstr ."') or (`status` like '" .$filterstr ."') or (`long` like '" .$filterstr ."') or (`lat` like '" .$filterstr ."') or (`org_code` like '" .$filterstr ."') or (`lp_machine_status` like '" .$filterstr ."') or (`org_hrm_name` like '" .$filterstr ."') or (`org_hrm_division_name` like '" .$filterstr ."') or (`org_hrm_district_name` like '" .$filterstr ."') or (`org_hrm_upazila_name` like '" .$filterstr ."') or (`org_hrm_org_type_name` like '" .$filterstr ."') or (`org_hrm_org_level_name` like '" .$filterstr ."') or (`org_hrm_org_photo` like '" .$filterstr ."') or (`org_hrm_land_phone1` like '" .$filterstr ."') or (`org_hrm_mobile_number1` like '" .$filterstr ."') or (`org_hrm_email_address1` like '" .$filterstr ."')";
  }
  $res = mysql_query($sql, $conn) or die(mysql_error());
  $row = mysql_fetch_assoc($res);
  reset($row);
  return current($row);
}

function sql_insert()
{
  global $conn;
  global $_POST;

  $sql = "insert into `hospitals` (`hosname`, `status`, `long`, `lat`, `org_code`, `machine_status`, `org_hrm_name`, `org_hrm_division_name`, `org_hrm_district_name`, `org_hrm_upazila_name`, `org_hrm_org_type_name`, `org_hrm_org_level_name`, `org_hrm_org_photo`, `org_hrm_land_phone1`, `org_hrm_mobile_number1`, `org_hrm_email_address1`) values (" .sqlvalue(@$_POST["hosname"], true).", " .sqlvalue(@$_POST["status"], false).", " .sqlvalue(@$_POST["long"], false).", " .sqlvalue(@$_POST["lat"], false).", " .sqlvalue(@$_POST["org_code"], true).", " .sqlvalue(@$_POST["machine_status"], false).", " .sqlvalue(@$_POST["org_hrm_name"], true).", " .sqlvalue(@$_POST["org_hrm_division_name"], true).", " .sqlvalue(@$_POST["org_hrm_district_name"], true).", " .sqlvalue(@$_POST["org_hrm_upazila_name"], true).", " .sqlvalue(@$_POST["org_hrm_org_type_name"], true).", " .sqlvalue(@$_POST["org_hrm_org_level_name"], true).", " .sqlvalue(@$_POST["org_hrm_org_photo"], true).", " .sqlvalue(@$_POST["org_hrm_land_phone1"], true).", " .sqlvalue(@$_POST["org_hrm_mobile_number1"], true).", " .sqlvalue(@$_POST["org_hrm_email_address1"], true).")";
  mysql_query($sql, $conn) or die(mysql_error());
}

function sql_update()
{
  global $conn;
  global $_POST;

  $sql = "update `hospitals` set `hosname`=" .sqlvalue(@$_POST["hosname"], true).", `status`=" .sqlvalue(@$_POST["status"], false).", `long`=" .sqlvalue(@$_POST["long"], false).", `lat`=" .sqlvalue(@$_POST["lat"], false).", `org_code`=" .sqlvalue(@$_POST["org_code"], true).", `machine_status`=" .sqlvalue(@$_POST["machine_status"], false).", `org_hrm_name`=" .sqlvalue(@$_POST["org_hrm_name"], true).", `org_hrm_division_name`=" .sqlvalue(@$_POST["org_hrm_division_name"], true).", `org_hrm_district_name`=" .sqlvalue(@$_POST["org_hrm_district_name"], true).", `org_hrm_upazila_name`=" .sqlvalue(@$_POST["org_hrm_upazila_name"], true).", `org_hrm_org_type_name`=" .sqlvalue(@$_POST["org_hrm_org_type_name"], true).", `org_hrm_org_level_name`=" .sqlvalue(@$_POST["org_hrm_org_level_name"], true).", `org_hrm_org_photo`=" .sqlvalue(@$_POST["org_hrm_org_photo"], true).", `org_hrm_land_phone1`=" .sqlvalue(@$_POST["org_hrm_land_phone1"], true).", `org_hrm_mobile_number1`=" .sqlvalue(@$_POST["org_hrm_mobile_number1"], true).", `org_hrm_email_address1`=" .sqlvalue(@$_POST["org_hrm_email_address1"], true) ." where " .primarykeycondition();
  mysql_query($sql, $conn) or die(mysql_error());
}
function primarykeycondition()
{
  global $_POST;
  $pk = "";
  $pk .= "(`hosid`";
  if (@$_POST["xhosid"] == "") {
    $pk .= " IS NULL";
  }else{
  $pk .= " = " .sqlvalue(@$_POST["xhosid"], false);
  };
  $pk .= ")";
  return $pk;
}
 ?>
