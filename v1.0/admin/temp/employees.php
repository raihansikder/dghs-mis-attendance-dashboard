<?php session_start();
  if (isset($_GET["order"])) $order = @$_GET["order"];
  if (isset($_GET["type"])) $ordtype = @$_GET["type"];

  if (isset($_POST["filter"])) $filter = @$_POST["filter"];
  if (isset($_POST["filter_field"])) $filterfield = @$_POST["filter_field"];
  $wholeonly = false;
  if (isset($_POST["wholeonly"])) $wholeonly = @$_POST["wholeonly"];

  if (!isset($order) && isset($_SESSION["order"])) $order = $_SESSION["order"];
  if (!isset($ordtype) && isset($_SESSION["type"])) $ordtype = $_SESSION["type"];
  if (!isset($filter) && isset($_SESSION["filter"])) $filter = $_SESSION["filter"];
  if (!isset($filterfield) && isset($_SESSION["filter_field"])) $filterfield = $_SESSION["filter_field"];

?>

<html>
<head>
<title>stagedgh_attendance -- employees</title>
<meta name="generator" http-equiv="content-type" content="text/html">
<style type="text/css">
  body {
    background-color: #FFFFFF;
    color: #004080;
    font-family: Arial;
    font-size: 12px;
  }
  .bd {
    background-color: #FFFFFF;
    color: #004080;
    font-family: Arial;
    font-size: 12px;
  }
  .tbl {
    background-color: #FFFFFF;
  }
  a:link { 
    background-color: #FFFFFF01;
    color: #FF0000;
    font-family: Arial;
    font-size: 12px;
  }
  a:active { 
    color: #0000FF;
    font-family: Arial;
    font-size: 12px;
  }
  a:visited { 
    color: #800080;
    font-family: Arial;
    font-size: 12px;
  }
  .hr {
    background-color: #336699;
    color: #FFFFFF;
    font-family: Arial;
    font-size: 12px;
  }
  a.hr:link {
    color: #FFFFFF;
    font-family: Arial;
    font-size: 12px;
  }
  a.hr:active {
    color: #FFFFFF;
    font-family: Arial;
    font-size: 12px;
  }
  a.hr:visited {
    color: #FFFFFF;
    font-family: Arial;
    font-size: 12px;
  }
  .dr {
    background-color: #FFFFFF;
    color: #000000;
    font-family: Arial;
    font-size: 12px;
  }
  .sr {
    background-color: #FFFFCF;
    color: #000000;
    font-family: Arial;
    font-size: 12px;
  }
</style>
</head>
<body>

<table width="100%">
<tr>

<td width="10%" valign="top">
<li><a href="districts.php?a=reset">districts</a>
<li><a href="divisions.php?a=reset">divisions</a>
<li><a href="hospitals.php?a=reset">hospitals</a>
<li><a href="machine_status.php?a=reset">machine_status</a>
</td>
<td width="5%">
</td>
<td bgcolor="#e0e0e0">
</td>
<td width="5%">
</td>
<td width="80%" valign="top">
<?php
  if (!login()) exit;
?>
<div style="float: right"><a href="employees.php?a=logout">[ Logout ]</a></div>
<br>
<?php
  $conn = connect();
  $showrecs = 20;
  $pagerange = 10;

  $a = @$_GET["a"];
  $recid = @$_GET["recid"];
  $page = @$_GET["page"];
  if (!isset($page)) $page = 1;

  $sql = @$_POST["sql"];

  switch ($sql) {
    case "insert":
      sql_insert();
      break;
    case "update":
      sql_update();
      break;
  }

  switch ($a) {
    case "add":
      addrec();
      break;
    case "view":
      viewrec($recid);
      break;
    case "edit":
      editrec($recid);
      break;
    default:
      select();
      break;
  }

  if (isset($order)) $_SESSION["order"] = $order;
  if (isset($ordtype)) $_SESSION["type"] = $ordtype;
  if (isset($filter)) $_SESSION["filter"] = $filter;
  if (isset($filterfield)) $_SESSION["filter_field"] = $filterfield;
  if (isset($wholeonly)) $_SESSION["wholeonly"] = $wholeonly;

  mysql_close($conn);
?>
</td></tr></table>

</body>
</html>

<?php function select()
  {
  global $a;
  global $showrecs;
  global $page;
  global $filter;
  global $filterfield;
  global $wholeonly;
  global $order;
  global $ordtype;


  if ($a == "reset") {
    $filter = "";
    $filterfield = "";
    $wholeonly = "";
    $order = "";
    $ordtype = "";
  }

  $checkstr = "";
  if ($wholeonly) $checkstr = " checked";
  if ($ordtype == "asc") { $ordtypestr = "desc"; } else { $ordtypestr = "asc"; }
  $res = sql_select();
  $count = sql_getrecordcount();
  if ($count % $showrecs != 0) {
    $pagecount = intval($count / $showrecs) + 1;
  }
  else {
    $pagecount = intval($count / $showrecs);
  }
  $startrec = $showrecs * ($page - 1);
  if ($startrec < $count) {mysql_data_seek($res, $startrec);}
  $reccount = min($showrecs * $page, $count);
?>
<table class="bd" border="0" cellspacing="1" cellpadding="4">
<tr><td>Table: employees</td></tr>
<tr><td>Records shown <?php echo $startrec + 1 ?> - <?php echo $reccount ?> of <?php echo $count ?></td></tr>
</table>
<hr size="1" noshade>
<form action="employees.php" method="post">
<table class="bd" border="0" cellspacing="1" cellpadding="4">
<tr>
<td><b>Custom Filter</b>&nbsp;</td>
<td><input type="text" name="filter" value="<?php echo $filter ?>"></td>
<td><select name="filter_field">
<option value="">All Fields</option>
<option value="<?php echo "hosid" ?>"<?php if ($filterfield == "hosid") { echo "selected"; } ?>><?php echo htmlspecialchars("hosid") ?></option>
<option value="<?php echo "empid" ?>"<?php if ($filterfield == "empid") { echo "selected"; } ?>><?php echo htmlspecialchars("empid") ?></option>
<option value="<?php echo "staffid" ?>"<?php if ($filterfield == "staffid") { echo "selected"; } ?>><?php echo htmlspecialchars("staffid") ?></option>
<option value="<?php echo "empname" ?>"<?php if ($filterfield == "empname") { echo "selected"; } ?>><?php echo htmlspecialchars("empname") ?></option>
<option value="<?php echo "designation" ?>"<?php if ($filterfield == "designation") { echo "selected"; } ?>><?php echo htmlspecialchars("designation") ?></option>
<option value="<?php echo "department" ?>"<?php if ($filterfield == "department") { echo "selected"; } ?>><?php echo htmlspecialchars("department") ?></option>
<option value="<?php echo "branch" ?>"<?php if ($filterfield == "branch") { echo "selected"; } ?>><?php echo htmlspecialchars("branch") ?></option>
<option value="<?php echo "email" ?>"<?php if ($filterfield == "email") { echo "selected"; } ?>><?php echo htmlspecialchars("email") ?></option>
<option value="<?php echo "phone" ?>"<?php if ($filterfield == "phone") { echo "selected"; } ?>><?php echo htmlspecialchars("phone") ?></option>
<option value="<?php echo "cell" ?>"<?php if ($filterfield == "cell") { echo "selected"; } ?>><?php echo htmlspecialchars("cell") ?></option>
<option value="<?php echo "status" ?>"<?php if ($filterfield == "status") { echo "selected"; } ?>><?php echo htmlspecialchars("status") ?></option>
<option value="<?php echo "staffcode" ?>"<?php if ($filterfield == "staffcode") { echo "selected"; } ?>><?php echo htmlspecialchars("staffcode") ?></option>
</select></td>
<td><input type="checkbox" name="wholeonly"<?php echo $checkstr ?>>Whole words only</td>
</td></tr>
<tr>
<td>&nbsp;</td>
<td><input type="submit" name="action" value="Apply Filter"></td>
<td><a href="employees.php?a=reset">Reset Filter</a></td>
</tr>
</table>
</form>
<hr size="1" noshade>
<?php showpagenav($page, $pagecount); ?>
<br>
<table class="tbl" border="0" cellspacing="1" cellpadding="5"width="100%">
<tr>
<td class="hr">&nbsp;</td>
<td class="hr">&nbsp;</td>
<td class="hr"><a class="hr" href="employees.php?order=<?php echo "hosid" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("hosid") ?></a></td>
<td class="hr"><a class="hr" href="employees.php?order=<?php echo "empid" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("empid") ?></a></td>
<td class="hr"><a class="hr" href="employees.php?order=<?php echo "staffid" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("staffid") ?></a></td>
<td class="hr"><a class="hr" href="employees.php?order=<?php echo "empname" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("empname") ?></a></td>
<td class="hr"><a class="hr" href="employees.php?order=<?php echo "designation" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("designation") ?></a></td>
<td class="hr"><a class="hr" href="employees.php?order=<?php echo "department" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("department") ?></a></td>
<td class="hr"><a class="hr" href="employees.php?order=<?php echo "branch" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("branch") ?></a></td>
<td class="hr"><a class="hr" href="employees.php?order=<?php echo "email" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("email") ?></a></td>
<td class="hr"><a class="hr" href="employees.php?order=<?php echo "phone" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("phone") ?></a></td>
<td class="hr"><a class="hr" href="employees.php?order=<?php echo "cell" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("cell") ?></a></td>
<td class="hr"><a class="hr" href="employees.php?order=<?php echo "status" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("status") ?></a></td>
<td class="hr"><a class="hr" href="employees.php?order=<?php echo "staffcode" ?>&type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("staffcode") ?></a></td>
</tr>
<?php
  for ($i = $startrec; $i < $reccount; $i++)
  {
    $row = mysql_fetch_assoc($res);
    $style = "dr";
    if ($i % 2 != 0) {
      $style = "sr";
    }
?>
<tr>
<td class="<?php echo $style ?>"><a href="employees.php?a=view&recid=<?php echo $i ?>">View</a></td>
<td class="<?php echo $style ?>"><a href="employees.php?a=edit&recid=<?php echo $i ?>">Edit</a></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["hosid"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["empid"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["staffid"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["empname"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["designation"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["department"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["branch"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["email"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["phone"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["cell"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["status"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["staffcode"]) ?></td>
</tr>
<?php
  }
  mysql_free_result($res);
?>
</table>
<br>
<?php showpagenav($page, $pagecount); ?>
<?php } ?>

<?php function login()
{
  global $_POST;
  global $_SESSION;

  global $_GET;
  if (isset($_GET["a"]) && ($_GET["a"] == 'logout')) $_SESSION["logged_in"] = false;
  if (!isset($_SESSION["logged_in"])) $_SESSION["logged_in"] = false;
  if (!$_SESSION["logged_in"]) {
    $login = "";
    $password = "";
    if (isset($_POST["login"])) $login = @$_POST["login"];
    if (isset($_POST["password"])) $password = @$_POST["password"];

    if (($login != "") && ($password != "")) {
      $conn = mysql_connect("localhost", "root", "");
      mysql_select_db("stagedgh_attendance");
      $sql = "select `account_password` from `account` where `account_email` = '" .$login ."'";
      $res = mysql_query($sql, $conn) or die(mysql_error());
      $row = mysql_fetch_assoc($res) or $row = array(0 => "");;
      if (isset($row)) reset($row);

      if (isset($password) && ($password == trim(current($row)))) {
        $_SESSION["logged_in"] = true;
    }
    else {
?>
<p><b><font color="-1">Sorry, the login/password combination you've entered is invalid</font></b></p>
<?php } } }if (isset($_SESSION["logged_in"]) && (!$_SESSION["logged_in"])) { ?>
<form action="employees.php" method="post">
<table class="bd" border="0" cellspacing="1" cellpadding="4">
<tr>
<td>Login</td>
<td><input type="text" name="login" value="<?php echo $login ?>"></td>
</tr>
<tr>
<td>Password</td>
<td><input type="password" name="password" value="<?php echo $password ?>"></td>
</tr>
<tr>
<td><input type="submit" name="action" value="Login"></td>
</tr>
</table>
</form>
<?php
  }
  if (!isset($_SESSION["logged_in"])) $_SESSION["logged_in"] = false;
  return $_SESSION["logged_in"];
} ?>

<?php function showrow($row, $recid)
  {
?>
<table class="tbl" border="0" cellspacing="1" cellpadding="5"width="50%">
<tr>
<td class="hr"><?php echo htmlspecialchars("hosid")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["hosid"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("empid")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["empid"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("staffid")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["staffid"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("empname")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["empname"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("designation")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["designation"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("department")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["department"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("branch")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["branch"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("email")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["email"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("phone")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["phone"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("cell")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["cell"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("status")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["status"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("staffcode")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["staffcode"]) ?></td>
</tr>
</table>
<?php } ?>

<?php function showroweditor($row, $iseditmode)
  {
  global $conn;
?>
<table class="tbl" border="0" cellspacing="1" cellpadding="5"width="50%">
<tr>
<td class="hr"><?php echo htmlspecialchars("hosid")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="hosid" value="<?php echo str_replace('"', '&quot;', trim($row["hosid"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("empid")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="empid" value="<?php echo str_replace('"', '&quot;', trim($row["empid"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("staffid")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="staffid" value="<?php echo str_replace('"', '&quot;', trim($row["staffid"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("empname")."&nbsp;" ?></td>
<td class="dr"><textarea cols="35" rows="4" name="empname" maxlength="255"><?php echo str_replace('"', '&quot;', trim($row["empname"])) ?></textarea></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("designation")."&nbsp;" ?></td>
<td class="dr"><textarea cols="35" rows="4" name="designation" maxlength="255"><?php echo str_replace('"', '&quot;', trim($row["designation"])) ?></textarea></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("department")."&nbsp;" ?></td>
<td class="dr"><textarea cols="35" rows="4" name="department" maxlength="255"><?php echo str_replace('"', '&quot;', trim($row["department"])) ?></textarea></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("branch")."&nbsp;" ?></td>
<td class="dr"><textarea cols="35" rows="4" name="branch" maxlength="255"><?php echo str_replace('"', '&quot;', trim($row["branch"])) ?></textarea></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("email")."&nbsp;" ?></td>
<td class="dr"><textarea cols="35" rows="4" name="email" maxlength="255"><?php echo str_replace('"', '&quot;', trim($row["email"])) ?></textarea></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("phone")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="phone" maxlength="15" value="<?php echo str_replace('"', '&quot;', trim($row["phone"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("cell")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="cell" maxlength="15" value="<?php echo str_replace('"', '&quot;', trim($row["cell"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("status")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="status" value="<?php echo str_replace('"', '&quot;', trim($row["status"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("staffcode")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="staffcode" value="<?php echo str_replace('"', '&quot;', trim($row["staffcode"])) ?>"></td>
</tr>
</table>
<?php } ?>

<?php function showpagenav($page, $pagecount)
{
?>
<table class="bd" border="0" cellspacing="1" cellpadding="4">
<tr>
<td><a href="employees.php?a=add">Add Record</a>&nbsp;</td>
<?php if ($page > 1) { ?>
<td><a href="employees.php?page=<?php echo $page - 1 ?>">&lt;&lt;&nbsp;Prev</a>&nbsp;</td>
<?php } ?>
<?php
  global $pagerange;

  if ($pagecount > 1) {

  if ($pagecount % $pagerange != 0) {
    $rangecount = intval($pagecount / $pagerange) + 1;
  }
  else {
    $rangecount = intval($pagecount / $pagerange);
  }
  for ($i = 1; $i < $rangecount + 1; $i++) {
    $startpage = (($i - 1) * $pagerange) + 1;
    $count = min($i * $pagerange, $pagecount);

    if ((($page >= $startpage) && ($page <= ($i * $pagerange)))) {
      for ($j = $startpage; $j < $count + 1; $j++) {
        if ($j == $page) {
?>
<td><b><?php echo $j ?></b></td>
<?php } else { ?>
<td><a href="employees.php?page=<?php echo $j ?>"><?php echo $j ?></a></td>
<?php } } } else { ?>
<td><a href="employees.php?page=<?php echo $startpage ?>"><?php echo $startpage ."..." .$count ?></a></td>
<?php } } } ?>
<?php if ($page < $pagecount) { ?>
<td>&nbsp;<a href="employees.php?page=<?php echo $page + 1 ?>">Next&nbsp;&gt;&gt;</a>&nbsp;</td>
<?php } ?>
</tr>
</table>
<?php } ?>

<?php function showrecnav($a, $recid, $count)
{
?>
<table class="bd" border="0" cellspacing="1" cellpadding="4">
<tr>
<td><a href="employees.php">Index Page</a></td>
<?php if ($recid > 0) { ?>
<td><a href="employees.php?a=<?php echo $a ?>&recid=<?php echo $recid - 1 ?>">Prior Record</a></td>
<?php } if ($recid < $count - 1) { ?>
<td><a href="employees.php?a=<?php echo $a ?>&recid=<?php echo $recid + 1 ?>">Next Record</a></td>
<?php } ?>
</tr>
</table>
<hr size="1" noshade>
<?php } ?>

<?php function addrec()
{
?>
<table class="bd" border="0" cellspacing="1" cellpadding="4">
<tr>
<td><a href="employees.php">Index Page</a></td>
</tr>
</table>
<hr size="1" noshade>
<form enctype="multipart/form-data" action="employees.php" method="post">
<p><input type="hidden" name="sql" value="insert"></p>
<?php
$row = array(
  "hosid" => "",
  "empid" => "",
  "staffid" => "",
  "empname" => "",
  "designation" => "",
  "department" => "",
  "branch" => "",
  "email" => "",
  "phone" => "",
  "cell" => "",
  "status" => "",
  "staffcode" => "");
showroweditor($row, false);
?>
<p><input type="submit" name="action" value="Post"></p>
</form>
<?php } ?>

<?php function viewrec($recid)
{
  $res = sql_select();
  $count = sql_getrecordcount();
  mysql_data_seek($res, $recid);
  $row = mysql_fetch_assoc($res);
  showrecnav("view", $recid, $count);
?>
<br>
<?php showrow($row, $recid) ?>
<br>
<hr size="1" noshade>
<table class="bd" border="0" cellspacing="1" cellpadding="4">
<tr>
<td><a href="employees.php?a=add">Add Record</a></td>
<td><a href="employees.php?a=edit&recid=<?php echo $recid ?>">Edit Record</a></td>
</tr>
</table>
<?php
  mysql_free_result($res);
} ?>

<?php function editrec($recid)
{
  $res = sql_select();
  $count = sql_getrecordcount();
  mysql_data_seek($res, $recid);
  $row = mysql_fetch_assoc($res);
  showrecnav("edit", $recid, $count);
?>
<br>
<form enctype="multipart/form-data" action="employees.php" method="post">
<input type="hidden" name="sql" value="update">
<input type="hidden" name="xhosid" value="<?php echo $row["hosid"] ?>">
<input type="hidden" name="xempid" value="<?php echo $row["empid"] ?>">
<?php showroweditor($row, true); ?>
<p><input type="submit" name="action" value="Post"></p>
</form>
<?php
  mysql_free_result($res);
} ?>

<?php function connect()
{
  $conn = mysql_connect("localhost", "root", "");
  mysql_select_db("stagedgh_attendance");
  return $conn;
}

function sqlvalue($val, $quote)
{
  if ($quote)
    $tmp = sqlstr($val);
  else
    $tmp = $val;
  if ($tmp == "")
    $tmp = "NULL";
  elseif ($quote)
    $tmp = "'".$tmp."'";
  return $tmp;
}

function sqlstr($val)
{
  return str_replace("'", "''", $val);
}

function sql_select()
{
  global $conn;
  global $order;
  global $ordtype;
  global $filter;
  global $filterfield;
  global $wholeonly;

  $filterstr = sqlstr($filter);
  if (!$wholeonly && isset($wholeonly) && $filterstr!='') $filterstr = "%" .$filterstr ."%";
  $sql = "SELECT `hosid`, `empid`, `staffid`, `empname`, `designation`, `department`, `branch`, `email`, `phone`, `cell`, `status`, `staffcode` FROM `employees`";
  if (isset($filterstr) && $filterstr!='' && isset($filterfield) && $filterfield!='') {
    $sql .= " where " .sqlstr($filterfield) ." like '" .$filterstr ."'";
  } elseif (isset($filterstr) && $filterstr!='') {
    $sql .= " where (`hosid` like '" .$filterstr ."') or (`empid` like '" .$filterstr ."') or (`staffid` like '" .$filterstr ."') or (`empname` like '" .$filterstr ."') or (`designation` like '" .$filterstr ."') or (`department` like '" .$filterstr ."') or (`branch` like '" .$filterstr ."') or (`email` like '" .$filterstr ."') or (`phone` like '" .$filterstr ."') or (`cell` like '" .$filterstr ."') or (`status` like '" .$filterstr ."') or (`staffcode` like '" .$filterstr ."')";
  }
  if (isset($order) && $order!='') $sql .= " order by `" .sqlstr($order) ."`";
  if (isset($ordtype) && $ordtype!='') $sql .= " " .sqlstr($ordtype);
  $res = mysql_query($sql, $conn) or die(mysql_error());
  return $res;
}

function sql_getrecordcount()
{
  global $conn;
  global $order;
  global $ordtype;
  global $filter;
  global $filterfield;
  global $wholeonly;

  $filterstr = sqlstr($filter);
  if (!$wholeonly && isset($wholeonly) && $filterstr!='') $filterstr = "%" .$filterstr ."%";
  $sql = "SELECT COUNT(*) FROM `employees`";
  if (isset($filterstr) && $filterstr!='' && isset($filterfield) && $filterfield!='') {
    $sql .= " where " .sqlstr($filterfield) ." like '" .$filterstr ."'";
  } elseif (isset($filterstr) && $filterstr!='') {
    $sql .= " where (`hosid` like '" .$filterstr ."') or (`empid` like '" .$filterstr ."') or (`staffid` like '" .$filterstr ."') or (`empname` like '" .$filterstr ."') or (`designation` like '" .$filterstr ."') or (`department` like '" .$filterstr ."') or (`branch` like '" .$filterstr ."') or (`email` like '" .$filterstr ."') or (`phone` like '" .$filterstr ."') or (`cell` like '" .$filterstr ."') or (`status` like '" .$filterstr ."') or (`staffcode` like '" .$filterstr ."')";
  }
  $res = mysql_query($sql, $conn) or die(mysql_error());
  $row = mysql_fetch_assoc($res);
  reset($row);
  return current($row);
}

function sql_insert()
{
  global $conn;
  global $_POST;

  $sql = "insert into `employees` (`hosid`, `empid`, `staffid`, `empname`, `designation`, `department`, `branch`, `email`, `phone`, `cell`, `status`, `staffcode`) values (" .sqlvalue(@$_POST["hosid"], false).", " .sqlvalue(@$_POST["empid"], false).", " .sqlvalue(@$_POST["staffid"], false).", " .sqlvalue(@$_POST["empname"], true).", " .sqlvalue(@$_POST["designation"], true).", " .sqlvalue(@$_POST["department"], true).", " .sqlvalue(@$_POST["branch"], true).", " .sqlvalue(@$_POST["email"], true).", " .sqlvalue(@$_POST["phone"], true).", " .sqlvalue(@$_POST["cell"], true).", " .sqlvalue(@$_POST["status"], false).", " .sqlvalue(@$_POST["staffcode"], false).")";
  mysql_query($sql, $conn) or die(mysql_error());
}

function sql_update()
{
  global $conn;
  global $_POST;

  $sql = "update `employees` set `hosid`=" .sqlvalue(@$_POST["hosid"], false).", `empid`=" .sqlvalue(@$_POST["empid"], false).", `staffid`=" .sqlvalue(@$_POST["staffid"], false).", `empname`=" .sqlvalue(@$_POST["empname"], true).", `designation`=" .sqlvalue(@$_POST["designation"], true).", `department`=" .sqlvalue(@$_POST["department"], true).", `branch`=" .sqlvalue(@$_POST["branch"], true).", `email`=" .sqlvalue(@$_POST["email"], true).", `phone`=" .sqlvalue(@$_POST["phone"], true).", `cell`=" .sqlvalue(@$_POST["cell"], true).", `status`=" .sqlvalue(@$_POST["status"], false).", `staffcode`=" .sqlvalue(@$_POST["staffcode"], false) ." where " .primarykeycondition();
  mysql_query($sql, $conn) or die(mysql_error());
}
function primarykeycondition()
{
  global $_POST;
  $pk = "";
  $pk .= "(`hosid`";
  if (@$_POST["xhosid"] == "") {
    $pk .= " IS NULL";
  }else{
  $pk .= " = " .sqlvalue(@$_POST["xhosid"], false);
  };
  $pk .= ") and ";
  $pk .= "(`empid`";
  if (@$_POST["xempid"] == "") {
    $pk .= " IS NULL";
  }else{
  $pk .= " = " .sqlvalue(@$_POST["xempid"], false);
  };
  $pk .= ")";
  return $pk;
}
 ?>
